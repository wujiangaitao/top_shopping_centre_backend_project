package top_shopping_centre.dto.cat;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class CatDTO {
    private Integer catId;
    private Integer feTopProductId;
    private Integer topCatProductQuantity;
    private String topCatProductSpecification;
    private Integer topCatProductStatus;
    private Integer topCatUserId;
}
