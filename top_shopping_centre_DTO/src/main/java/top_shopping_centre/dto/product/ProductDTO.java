package top_shopping_centre.dto.product;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class ProductDTO {
    private Integer id;
}
