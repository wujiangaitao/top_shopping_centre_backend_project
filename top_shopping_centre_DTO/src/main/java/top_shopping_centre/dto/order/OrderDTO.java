package top_shopping_centre.dto.order;

import lombok.Data;
import top_shopping_centre.dto.pay.PayProductDTO;

import java.util.List;

/**
 * @author jiangfeng
 */
@Data
public class OrderDTO {
    private Integer[] catId;
    private Integer[] orderId;
    private Integer status;
    private List<PayProductDTO> productDTOList;
    private Integer userId;
    private Integer catStatus;
    private String topOrderUserProvince;
    private String topOrderUserCity;
    private String topOrderUserCounty;
    private String topOrderUserDetailed;
    private String topOrderUserName;
    private String topOrderUserPhone;
    private Integer id;
}
