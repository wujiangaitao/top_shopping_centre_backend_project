package top_shopping_centre.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * @author jiangfeng
 */
@Data
public class UserLoginDTO {
    @Size(min=6,max=10,message = "账号不能少于6位")
    @NotBlank
    private String userLoginName;
    @Size(min=6,max=10,message = "密码不能少于6位")
    @NotBlank
    private String userLoginPwd;
    private String userLoginStatus;
    private LocalDateTime gmtUserLoginData;
}
