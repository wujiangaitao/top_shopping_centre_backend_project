package top_shopping_centre.dto.user;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author jiangfeng
 */
@Data
public class UserInfoDTO {
    private Integer userId;
    private String username;
    private String uname;
    @Size(min = 18, message = "身份证不能少于18位")
    private String identityCard;
    @Size(min = 11, message = "手机号不能少于11位")
    private String phone;
    private Character gender;
    private MultipartFile file;
}
