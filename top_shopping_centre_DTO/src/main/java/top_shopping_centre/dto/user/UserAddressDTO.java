package top_shopping_centre.dto.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @author jiangfeng
 */
@Data
public class UserAddressDTO {
    private Integer id;
    private String userAddressProvince;
    private String userAddressCity;
    private String userAddressCounty;
    private String userAddressDetailed;
    private String userAddressName;
    @Size(min = 11,message = "手机号不能少于11位")
    @NotBlank
    private String userAddressPhone;
    private Integer userAddressDefault;
    private Integer feUserId;
    private Integer userAddressCancel;
}
