package top_shopping_centre.dto.pay;

import lombok.Data;


/**
 * @author jiangfeng
 * 支付宝
 */
@Data
public class PayDTO {
    private Integer total;
}
