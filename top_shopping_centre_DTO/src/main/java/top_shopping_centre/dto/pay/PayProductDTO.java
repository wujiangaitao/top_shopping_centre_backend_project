package top_shopping_centre.dto.pay;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jiangfeng
 */
@Data
public class PayProductDTO {
    private Integer catId;
    private String topProductDesc;
    private String topProductName;
    private Integer quantity;
    private BigDecimal topProductPrice;
    private Integer status;
    private Integer userId;
}
