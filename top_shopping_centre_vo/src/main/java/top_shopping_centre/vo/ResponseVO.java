package top_shopping_centre.vo;


import lombok.Data;

/**
 * @author jiangfeng
 * 统一回复前端
 */
@Data
public class ResponseVO{
    private Integer code;
    private String msg;
    private Object data;


    public ResponseVO(Integer code, String msg) {
        this.code=code;
        this.msg=msg;
    }

    public ResponseVO(Integer code,String msg,Object data) {
        this.code=code;
        this.msg=msg;
        this.data=data;
    }


}
