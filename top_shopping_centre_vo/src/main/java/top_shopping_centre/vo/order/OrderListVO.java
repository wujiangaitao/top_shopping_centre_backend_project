package top_shopping_centre.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.order.OrderEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class OrderListVO {
    private List<OrderEntity> orderEntityList;
}
