package top_shopping_centre.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.order.OrderEntity;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class OrderVO {
    private OrderEntity orderEntity;
}
