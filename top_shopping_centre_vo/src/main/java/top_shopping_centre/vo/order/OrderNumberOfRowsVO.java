package top_shopping_centre.vo.order;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class OrderNumberOfRowsVO {
    private Integer numberOfRows;
}
