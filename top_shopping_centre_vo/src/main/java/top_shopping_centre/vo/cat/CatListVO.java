package top_shopping_centre.vo.cat;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.cat.CatEntity;
import java.util.List;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class CatListVO {
    private List<CatEntity> catEntityList;
}
