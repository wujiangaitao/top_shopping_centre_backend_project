package top_shopping_centre.vo.cat;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class CatNumberOfRowsVO {
    private Integer numberOfRows;
}
