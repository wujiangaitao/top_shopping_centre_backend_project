package top_shopping_centre.vo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.user.UserEntity;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class UserInfoVO {
    private UserEntity userEntity;
}
