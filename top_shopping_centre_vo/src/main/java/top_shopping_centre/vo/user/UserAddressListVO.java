package top_shopping_centre.vo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.user.useraddress.UserAddressEntity;
import java.util.List;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class UserAddressListVO {
    private List<UserAddressEntity> userAddressEntity;
}
