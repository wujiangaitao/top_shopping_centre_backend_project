package top_shopping_centre.vo.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.user.useraddress.UserAddressEntity;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class UserAddressVO {
    private UserAddressEntity userAddressEntity;
}
