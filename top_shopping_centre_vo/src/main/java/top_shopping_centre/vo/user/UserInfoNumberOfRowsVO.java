package top_shopping_centre.vo.user;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class UserInfoNumberOfRowsVO {
    private Integer numberOfRows;
}
