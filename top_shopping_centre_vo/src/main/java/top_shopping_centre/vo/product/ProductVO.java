package top_shopping_centre.vo.product;


import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.product.ProductEntity;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class ProductVO {
    private ProductEntity productEntity;
}
