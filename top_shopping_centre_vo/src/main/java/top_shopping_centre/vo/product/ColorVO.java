package top_shopping_centre.vo.product;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.entity.product.color.ColorEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class ColorVO {
    private List<ColorEntity> colorEntity;
}
