package top_shopping_centre.util;

/**
 * @author jiangfeng
 * 全局静态字段访问类
 */
public abstract class Constants {

    /**
     * 日期
     * */
    public static class Date {
        public static final String DATE_PATTERN="yyyy-MM-dd";
        public static final String TIME_PATTERN="yyyy-MM-dd";
        public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    }

    /**
     * rabbit
     * */
    public static class Mq {

    }

    /**
     * Es
     * */
    public static class Es {

    }

    /**
     * zbf
     * */
    public static class zbf{
        public static final String PRIVATE_KEY="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCEwyzouKTFaeA87WQuyZQiO7A/jhdVkcf/7DZRQZo4AUjloM3TpBYobzdtwvOKPwvqrX1GHhXzrhtz2QDXVYVxya4ISffo6JZ6WxtfYScwaeP1pz02aRk+OBPK44z6jfQdBkG+22ZExWKCQhVCqhSNgOKuPZy0IBsHH2zRAxbmMX411uwJi6+GG4kbRBefgu/E4da2dBMhxrS2beNJ6mMnqnVNuPX/npsbEaLTsyPoSJ4wUMjJzxNvDe1cpg/Bz2ZmMTGzHmyxgOjldVnybxER15uJjZuwoK2cFLNDbdbBoiuIsuKbSTVZgWW7T4fHE3AJA2IlodsN1pqV/0ttOSZpAgMBAAECggEAGf0O5HuInbBtGeAqlVIWcmEWzkolXTXNXdbCh0N9SetOdsz0nbJoOAAVbKIWBID09zqA5GHtUCdL7nCTTpPKLI0bSToZknk9Q7Rj9lbRUhPYcPUM8YK/U0Os9DF5Hm79U1UvN4pBScMN1ovTF5qtrUmGli8vZr9GKy3Gol+lcugDDoNZe6KrTh3Tl/9VWb0/6mDIwifX/qi/C3eyfMh/DSlM9k99r0Ddiqspmg/1vhmPxv+X8+YtYu9OXrqaRhKipwEnYrJwY5UE8k9ZBkNDCzaxL73jDBuk1hLODDpPnHcbXkUsUfQimlkfeYE69iLfTKWM1GiGvh1xQK4zvm5ztQKBgQDDJe0ihzvrQKuFnODZTdFSuf5q0xOSPaIQ17aCvzQpMOYf3n125KApL0sl04qJmjHR2T6kLZyf5LB7e+pi5fZiNKKCELbNr/bRBrTmReIC2BVkJjahGCNvG6N3NKP9DFxCraPAVEFz/60CyEvnP5ue/mu2uq1aX3+IcghGfNm3owKBgQCuKTAWg84fITJwXtxNXRN1a3ShNWYq6nlViw0QWpnk1/g70f/kQumYqZx1RgyM2rRS4lP0cww2Vj/OTsjJFNIJpf9X0PuVjAmYA2IE5I7x9cxD/aYQXc7wrwYig9YLx3UgPh+0XSvIU09U3l3pxqDVvYeyPj/PHEmN25RvJUn6gwKBgCpA1f155oFjeT/u3chvTvloiCu9FCm/6o1XB497suGQUuNnmybZDcydDtpb2U0987eKIEQQFC+Sppu2EuPjAtv0pZMf51TV2s0bdKO/UdZnzwnmNEHhJhrykwxGe5NYz8koK6KDG8DwBRfrInQpk5pzYAMqnEpLDyQN/Li2Y7WdAoGBAILrQVPbdunduZ3RIUgirIr64OyCSLAAHH+ajJJ7mO33D4pgXTzIs/LnpU5e/vYvPZiEvFmo1khOY56BiNvng9Bu8VihJ04Ou/OCr39AFNh8TjXaCaiJ7BLvbuwpMJEgOcKoEIsz8sKfJaaFlhEzGBo0ZDowIyVcfUpfAuDeJhNjAoGAQTFcsDIq8g+ArUxrtCknqO2UeKqiFR6e8+RBa26wpGxICWp6ce0GpdA5DIw3P5R3JDMdHPZ1Y2Og7We68VWcuJWKBDD+ovejtDjmwbwklNow56gdMKUcMqRK8KD6ZukwSdxH66EDSymnITsBSr6xd+qixNXvDxlTwHjHk+EPXCY=";
        public static final String FORMAT="json";
        public static final String SIGN_TYPE="RSA2";
        public static final String CHARSET="UTF-8";
        public static final String APPID="9021000122696276";
        public static final String SERVER_URL="https://openapi-sandbox.dl.alipaydev.com/gateway.do";
        public static final String ALIPAY_PUBLIC_KEY="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqCxAHlAAUjYao6ELkxk+GZbubB4F2+05M2W0fzbG8NGUnB6OWBS/CkDDKU1L0KTJAt5W3WkTi9FQ03scQ8TihjIWsGjRpGDrb5g3f7M1mITxKOhTRFsrhqHHlZX2Y4Idxm5rKj+a0TDff+JjkF5+jN2u1eU1C+una8UZwBU20xQXNHjJ1NuFGbfjKNtYDK7gpUc9NOQvhy4LjiulA1AyjAK7IBqe7WNqDJi64rWnk8fmUQmQm4nKgu4dSYoUp+G/xQ8lNWXYzFSKkD3i8BDv6XkDILEj/Q0P9JS3AbHxtpX2EBiLXWRfHhvhPklGP89WqfoXUEFu//Mc7ovTSpUZSQIDAQAB";
    }

    /**
     * minio
     * */
    public static class minio{
        public static final String USER_BUCKET ="userimg";
        public static final String BUCKET_NAME_M ="mgimage";
        public static final String BUCKET_NAME_W ="wrimage";
    }
}
