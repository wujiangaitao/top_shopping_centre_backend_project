package top_shopping_centre.util;


import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import top_shopping_centre.dto.pay.PayDTO;

import java.text.SimpleDateFormat;
import java.util.Date;

import static top_shopping_centre.util.Constants.zbf.*;

/**
 * @author jiangfeng
 * 支付宝支付
 */
public abstract class ZFBPayUtils {
    public static String zfb(PayDTO payDTO) {
        AlipayClient alipayClient =
                new DefaultAlipayClient(
                        SERVER_URL,
                        APPID,
                        PRIVATE_KEY,
                        FORMAT,
                        CHARSET,
                        ALIPAY_PUBLIC_KEY,
                        SIGN_TYPE);
        AlipayTradePagePayRequest request = getAlipayTradePagePayRequest(payDTO);
        AlipayTradePagePayResponse response;
        try {
            response = alipayClient.pageExecute(request);
        } catch (AlipayApiException e) {
            throw new RuntimeException(e);
        }
        if (response.isSuccess()) {
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
        return response.getBody();
    }

    private static AlipayTradePagePayRequest getAlipayTradePagePayRequest(PayDTO payDTO) {
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //异步接收地址，仅支持http/https，公网可访问
        request.setNotifyUrl("http://127.0.0.1:5500/html/cat.html");
        //同步跳转地址，仅支持http/https
        request.setReturnUrl("http://127.0.0.1:5500/html/cat.html");

        //必传参数
        JSONObject bizContent = new JSONObject();
        //商户订单号，商家自定义，保持唯一性20210817010101004
        bizContent.put("out_trade_no", getTimeStamp());
        //支付金额，最小值0.01元
        bizContent.put("total_amount", payDTO.getTotal());
        //订单标题，不可使用特殊符号
        bizContent.put("subject", "江辰玩具商城订单");
        //电脑网站支付场景固定传值FAST_INSTANT_TRADE_PAY
        bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");

        request.setBizContent(bizContent.toString());
        return request;
    }


    private static String getTimeStamp() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        Date date = new Date();
        return simpleDateFormat.format(date);
    }

}
