package top_shopping_centre.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.UUID;

/**
 * @author jiangfeng
 * JWT创建和验证
 */
public abstract class JwtUtils {
    /**
     * SIGNATURE:签名
     * USE:主题
     * algorithm:算法加密
     * DATE:创建时间
     * DATEGoQI:过期时间
     */
    private static  final String SIGNATURE="jc";
    private static  final String USE="login";
    private static  final Algorithm ALGORITHM = Algorithm.HMAC256("jc");
    private static  final Date DATE=new Date();
    private static  final Date DATE_G_OQI =new Date(System.currentTimeMillis() + 500000L);


    /**
     * 创建
     * */
    public static String createJwt(){
        return JWT.create()
                .withIssuer(SIGNATURE)
                .withSubject(USE)
                .withIssuedAt(DATE)
                .withJWTId(UUID.randomUUID().toString())
                .withExpiresAt(DATE_G_OQI)
                .sign(ALGORITHM);
    }

    /**
     * 验证
     * */
    public static DecodedJWT verifyJwt(String token){
        JWTVerifier build = JWT.require(ALGORITHM).build();
        return build.verify(token);
    }
}
