package top_shopping_centre.util;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import top_shopping_centre.exception.ComparisonException;

import java.util.List;

/**
 * @author jiangfeng
 */
public abstract class ComparisonUtils {

    public static void getResult(BindingResult result){
        List<FieldError> fieldErrors = result.getFieldErrors();
        String results = null;
        for (FieldError fieldError : fieldErrors) {
            results=fieldError.getDefaultMessage();
        }
        if (result.hasErrors()){
            throw new ComparisonException(results);
        }
    }
}
