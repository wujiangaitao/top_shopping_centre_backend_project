package top_shopping_centre.exception;

import lombok.Getter;

/**
 * @author jiangfeng
 */
@Getter
public class ComparisonException  extends RuntimeException{
    private String msg;

    public ComparisonException(String msg){
        this.msg=msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
