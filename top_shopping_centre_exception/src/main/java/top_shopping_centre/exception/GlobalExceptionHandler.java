package top_shopping_centre.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import top_shopping_centre.vo.ResponseVO;
import static top_shopping_centre.vo.EncapsulationResponseVO.fail;

/**
 * @author jiangfeng
 * 全局异常处理
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ComparisonException.class)
    public ResponseVO handleException(ComparisonException comparisonException){
        return fail(comparisonException.getMsg());
    }
}
