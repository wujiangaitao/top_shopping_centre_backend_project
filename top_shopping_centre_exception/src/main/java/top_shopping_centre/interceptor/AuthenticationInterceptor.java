package top_shopping_centre.interceptor;


import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Objects;


/**
 * @author jiangfeng
 * 拦截器：登录
 */
public class AuthenticationInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if ("userId".equals(headerName)){
                String header = request.getHeader(headerName);
              if (Objects.equals(header, "null") || header.isEmpty()){
                  try {
                      response.sendError(HttpServletResponse.SC_BAD_GATEWAY,"你还为登录，请先登录!");
                  } catch (IOException e) {
                      throw new RuntimeException(e);
                  }
                  return false;
              }
            }
        }
        return true;
    }
}
