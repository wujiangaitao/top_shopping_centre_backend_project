package top_shopping_centre.mapper;

import top_shopping_centre.entity.user.userlogin.UserLoginEntity;

/**
 * @author jiangfeng
 */
public interface UserLoginMapper {
    /**
     * 添加登录用户
     * */
    int insertUserLoginInformation(UserLoginEntity userLoginEntity);


    /**
     * 查询用户是否存在
     * */
    UserLoginEntity selectUserExist(UserLoginEntity userLoginEntity);

    /**
     * 修改用户名
     * */
    int updateUserLoginInformation(UserLoginEntity userLoginEntity);
}
