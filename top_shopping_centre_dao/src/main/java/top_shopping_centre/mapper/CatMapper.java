package top_shopping_centre.mapper;

import top_shopping_centre.entity.cat.CatEntity;

import java.util.List;

/**
 * @author jiangfeng
 * 购物车
 */
public interface CatMapper {
    /**
     * 添加购物车
     * */
    int insertCat(CatEntity catEntity);

    /**
     * 查询购物车是否存在该商品
     * */
    List<CatEntity> selectCatProduct(CatEntity catEntity);

    /**
     * 修改购物车数量:++
     * */
    int updateCatProductJia(CatEntity catEntity);

    /**
     * 修改购物车数量:--
     * */
    int updateCatProductJan(CatEntity catEntity);

    /**
     * 查询购物车所有商品
     * */
    List<CatEntity> selectCatAllProduct(CatEntity catEntity);

    /**
     * 删除购物车
     */
    int deleteCatProduct(CatEntity catEntity);

    /**
     * 批量修改
     * */
    int updateCatStatus(List<CatEntity> catEntity);


}
