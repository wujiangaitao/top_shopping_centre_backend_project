package top_shopping_centre.mapper;


import top_shopping_centre.entity.user.useraddress.UserAddressEntity;

import java.util.List;


/**
 * @author jiangfeng
 * 用户
 */
public interface UserAddressMapper {
    /**
     * 添加用户地址
     * */
    int insertUserAddress(UserAddressEntity userAddressEntity);

    /**
     * 查询用户地址通过用户id
     * */
    List<UserAddressEntity> selectUserAddressList(UserAddressEntity userAddressEntity);

    /**
     * 删除地址通过用户id
     * */
    int deleteUserAddress(UserAddressEntity userAddressEntity);

    /**
     * 用户设置为默认地址
     * */
    int setTheDefault(UserAddressEntity userAddressEntity);

    /**
     * 查找用户是否设置了默认地址
     * */
    UserAddressEntity selectUserAddressDefault(UserAddressEntity userAddressEntity);



}
