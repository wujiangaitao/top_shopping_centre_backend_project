package top_shopping_centre.mapper;

import top_shopping_centre.entity.order.OrderEntity;

import java.util.List;


/**
 * @author jiangfeng
 * 订单
 */
public interface OrderMapper {
    /**
     * 批量添加到订单表中
     * */
    int insertOrder(List<OrderEntity> orderEntity);

    /**
     * 查询所有预支付的订单
     * */
    List<OrderEntity> userOrderProduct(OrderEntity orderEntity);

    /**
     * 修改订单表中状态
     * */
    int updateOrderStatus(List<OrderEntity> orderEntity);

    /**
     * 查询用户当前的所有订单
     * */
    List<OrderEntity> selectUserOrderListAndUserId(OrderEntity orderEntity);

    /**
     * 取消支付
     * */
    int userCancelThePaymentOrder(OrderEntity orderEntity);

    /**
     * 用户传入id单个用户购买信息
     */
    OrderEntity userOrderIdInfo(OrderEntity orderEntity);
}
