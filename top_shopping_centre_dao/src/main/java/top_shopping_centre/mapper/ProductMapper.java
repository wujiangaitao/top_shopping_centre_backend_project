package top_shopping_centre.mapper;

import top_shopping_centre.entity.product.color.ColorEntity;
import top_shopping_centre.entity.product.ProductEntity;
import top_shopping_centre.entity.product.image.ProductImageEntity;

import java.util.List;

/**
 * @author jiangfeng
 * 商品
 */
public interface ProductMapper {
    /**
     * 查询所有商品
     * */
    List<ProductEntity> getAllProduct();

    /**
     * 查询通过id查询商品
     * */
    ProductEntity getProductById(ProductEntity productEntity);

    /**
     * 通过商品id查询该商品的所有颜色
     * */
    List<ColorEntity> getProductColor(ProductEntity productEntity);


    /**
     * 通过商品id查询该商品的所有图片
     * */
    List<ProductImageEntity> getProductAllImage(ProductEntity productEntity);

    /**
     * 通过id查询该商品
     * */
    List<ProductEntity> selectProductId(Integer id);

}
