package top_shopping_centre.mapper;

import top_shopping_centre.entity.user.UserEntity;
import top_shopping_centre.entity.user.userlogin.UserLoginEntity;



/**
 * @author jiangfeng
 * 用户
 */
public interface UserMapper {

    /**
     * 添加用户信息
     * */
    int insertUserInformation(UserEntity userEntity);

    /**
     * 查询用户信息
     * */
    UserEntity selectUserInformation(UserEntity userEntity);

    /**
     * 查询用户的登录账号
     * */
    UserLoginEntity selectUserLoginInfo(UserLoginEntity userLoginEntity);

    /**
     * 完整信息
     * */
    int updateUserInfo(UserEntity userEntity);


}
