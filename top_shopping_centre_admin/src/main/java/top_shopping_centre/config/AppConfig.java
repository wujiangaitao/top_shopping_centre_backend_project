package top_shopping_centre.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author jiangfeng
 * 主配置类
 */
@Configuration
@EnableWebMvc
@ComponentScan("top_shopping_centre.com")
@Import({MybatisConfig.class,MinioConfig.class})
public class AppConfig {}
