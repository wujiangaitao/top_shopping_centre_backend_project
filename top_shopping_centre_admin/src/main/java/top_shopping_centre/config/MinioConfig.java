package top_shopping_centre.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

/**
 * @author jiangfeng
 * minio
 */
@Configuration
@PropertySource("classpath:minio.properties")
public class MinioConfig {

    @Value("${minio.endpoint}")
    private String endpoint;

    @Value("${minio.accessKey}")
    private String accessKey;

    @Value("${minio.secretKey}")
    private String secretKey;

    @Bean
    public MinioClient createMinio(){
        return MinioClient.builder().endpoint(endpoint).credentials(accessKey,secretKey).build();
    }

    @Bean
    public MultipartResolver multipartResolver() {
        //这个必须要有这个 multipartResolver 名字的 MultipartResolver 对象实例给到 spring 框架，
        //不然在上传文件时有其他的内容时会报错，其他类型的属性无法被赋值
        return new StandardServletMultipartResolver();
    }

}
