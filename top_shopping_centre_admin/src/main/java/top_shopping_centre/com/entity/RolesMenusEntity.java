package top_shopping_centre.com.entity;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class RolesMenusEntity {
    private Integer rmId;
    private Integer rId;
    private Integer mId;
}
