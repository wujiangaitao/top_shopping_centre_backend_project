package top_shopping_centre.com.entity;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class BrandEntity {
    private Integer id;
    private String topBrandName;
    private String topBrandImage;
    private Integer topBrandStatus;
}
