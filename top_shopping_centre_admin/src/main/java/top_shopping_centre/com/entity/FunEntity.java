package top_shopping_centre.com.entity;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class FunEntity {
    private Integer fId;
    private String tFunName;
    private String tFunUrl;
}
