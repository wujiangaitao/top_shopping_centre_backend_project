package top_shopping_centre.com.entity;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class ColorEntity {
    private Integer id;
    private String topColorName;
}
