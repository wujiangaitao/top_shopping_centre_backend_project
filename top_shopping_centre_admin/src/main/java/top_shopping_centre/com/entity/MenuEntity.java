package top_shopping_centre.com.entity;



import java.util.List;

/**
 * @author jiangfeng
 */
public class MenuEntity {
    private Integer mId;
    private String tMenuName;
    private String tMenuUrl;
    private Integer tMenuFatherId;
    List<MenuEntity> zi;

    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    public String gettMenuName() {
        return tMenuName;
    }

    public void settMenuName(String tMenuName) {
        this.tMenuName = tMenuName;
    }

    public String gettMenuUrl() {
        return tMenuUrl;
    }

    public void settMenuUrl(String tMenuUrl) {
        this.tMenuUrl = tMenuUrl;
    }

    public Integer gettMenuFatherId() {
        return tMenuFatherId;
    }

    public void settMenuFatherId(Integer tMenuFatherId) {
        this.tMenuFatherId = tMenuFatherId;
    }


    public List<MenuEntity> getZi() {
        return zi;
    }

    public void setZi(List<MenuEntity> zi) {
        this.zi = zi;
    }

    @Override
    public String toString() {
        return "MenuEntity{" +
                "mId=" + mId +
                ", tMenuName='" + tMenuName + '\'' +
                ", tMenuUrl='" + tMenuUrl + '\'' +
                ", tMenuFatherId=" + tMenuFatherId +
                ", zi=" + zi +
                '}';
    }
}
