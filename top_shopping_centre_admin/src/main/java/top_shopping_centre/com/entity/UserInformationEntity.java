package top_shopping_centre.com.entity;

import lombok.Data;

/**
 * @author jiangfeng
 * 用户信息表
 */
@Data
public class UserInformationEntity {
    private Integer uId;
    private String tUserInformationName;
    private String tUserInformationPassword;
    private Integer tRoleId;
    private RoleEntity roleEntity;
}
