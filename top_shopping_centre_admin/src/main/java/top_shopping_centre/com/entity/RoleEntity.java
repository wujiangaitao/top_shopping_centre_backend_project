package top_shopping_centre.com.entity;

import lombok.Data;

/**
 * @author jiangfeng
 * 角色表
 */
@Data
public class RoleEntity {
    private Integer rId;
    private String tRoleName;
}
