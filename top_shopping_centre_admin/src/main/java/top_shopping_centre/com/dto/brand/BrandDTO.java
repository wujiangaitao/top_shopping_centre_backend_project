package top_shopping_centre.com.dto.brand;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class BrandDTO {
    private Integer id;
    private Integer status;
}
