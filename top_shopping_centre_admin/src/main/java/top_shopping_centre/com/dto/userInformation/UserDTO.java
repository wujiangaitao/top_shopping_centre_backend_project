package top_shopping_centre.com.dto.userInformation;





/**
 * @author jiangfeng
 */
public class UserDTO {
    private String tUserInformationName;
    private String tUserInformationPassword;

    public String gettUserInformationName() {
        return tUserInformationName;
    }

    public void settUserInformationName(String tUserInformationName) {
        this.tUserInformationName = tUserInformationName;
    }

    public String gettUserInformationPassword() {
        return tUserInformationPassword;
    }

    public void settUserInformationPassword(String tUserInformationPassword) {
        this.tUserInformationPassword = tUserInformationPassword;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "tUserInformationName='" + tUserInformationName + '\'' +
                ", tUserInformationPassword='" + tUserInformationPassword + '\'' +
                '}';
    }
}
