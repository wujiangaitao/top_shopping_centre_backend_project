package top_shopping_centre.com.dto.rolesmenus;

import lombok.Data;

/**
 * @author jiangfeng
 */
public class RoleDTO {
    private Integer rId;

    public Integer getrId() {
        return rId;
    }

    public void setrId(Integer rId) {
        this.rId = rId;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
                "rId=" + rId +
                '}';
    }
}
