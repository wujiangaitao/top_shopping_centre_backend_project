package top_shopping_centre.com.dto.color;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class ColorDTO {
    private Integer colorId;
    private String colorName;
}
