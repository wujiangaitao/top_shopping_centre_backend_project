package top_shopping_centre.com.mapper;

import top_shopping_centre.com.entity.ColorEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
public interface ColorMapper {

    /**
     * 查询所有颜色
     * */
    List<ColorEntity> colorList();

    /**
     * 添加颜色信息
     * */
    int insertColor(ColorEntity colorEntity);

    /**
     * 删除颜色信息
     * */
    int deleteColor(ColorEntity colorEntity);

    /**
     * 查询颜色是否存在
     * */
    ColorEntity selectColorExist(ColorEntity colorEntity);
}
