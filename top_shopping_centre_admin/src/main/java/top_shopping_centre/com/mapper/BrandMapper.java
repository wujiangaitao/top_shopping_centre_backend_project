package top_shopping_centre.com.mapper;


import top_shopping_centre.com.entity.BrandEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
public interface BrandMapper {

    /**
     * 查询所有品牌
     * */
    List<BrandEntity> selectBrandList();

    /**
     * 修改品牌状态
     * */
    int updateBrandStatus(BrandEntity brandEntity);

    /**
     * 删除品牌
     * */
    int deleteBrand(BrandEntity brandEntity);
}
