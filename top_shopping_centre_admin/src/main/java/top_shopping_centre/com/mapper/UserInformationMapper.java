package top_shopping_centre.com.mapper;

import top_shopping_centre.com.entity.UserInformationEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
public interface UserInformationMapper {

    /**
     * 登录信息查询
     * */
    UserInformationEntity loginUserInfo(UserInformationEntity userInformationEntity);

    /**
     * 查询所以用户信息
     * */
    List<UserInformationEntity> selectListAllUserInfo();
}
