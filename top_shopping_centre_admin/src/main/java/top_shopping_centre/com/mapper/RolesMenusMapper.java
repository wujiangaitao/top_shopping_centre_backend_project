package top_shopping_centre.com.mapper;

import top_shopping_centre.com.entity.MenuEntity;
import top_shopping_centre.com.entity.RolesMenusEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
public interface RolesMenusMapper {

    List<MenuEntity> selectRolesMenus(RolesMenusEntity rolesMenusEntity);
}
