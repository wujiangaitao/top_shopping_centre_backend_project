package top_shopping_centre.com.mapper;

import top_shopping_centre.com.entity.MenuEntity;

import java.util.List;

/**
 * @author jiangfeng
 * 权限菜单查询
 */
public interface MenusMapper {

    /**
     * 查询所有的菜单
     * */
    List<MenuEntity> selectAllMenu();




}
