package top_shopping_centre.com.interceptor;


import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.HandlerInterceptor;
import top_shopping_centre.com.util.JwtUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jiangfeng
 * 拦截器：判断是否登录过
 */
public class AuthenticationInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) {
        String authorization = request.getHeader("Authorization");
        System.out.println(authorization);
        return JwtUtils.verifyJwt(authorization)!=null;
    }
}
