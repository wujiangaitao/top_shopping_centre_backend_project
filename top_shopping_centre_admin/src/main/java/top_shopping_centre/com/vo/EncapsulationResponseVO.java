package top_shopping_centre.com.vo;

/**
 * @author jiangfeng
 * 封装ResponseVO对象
 */
public class  EncapsulationResponseVO{

    private EncapsulationResponseVO() {
    }

    public static ResponseVO succeed(Object data){
        return new ResponseVO(200,"ok",data);
    }

    public static  ResponseVO fail(String msg){
        return new ResponseVO(600,msg);
    }
}
