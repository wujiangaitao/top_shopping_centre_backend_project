package top_shopping_centre.com.vo.rolesmenus;

import lombok.Data;
import top_shopping_centre.com.entity.MenuEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
@Data
public class MenuVO {
    private List<MenuEntity> menuVOList;
    public MenuVO(List<MenuEntity> menuVOList) {
        this.menuVOList = menuVOList;
    }
}
