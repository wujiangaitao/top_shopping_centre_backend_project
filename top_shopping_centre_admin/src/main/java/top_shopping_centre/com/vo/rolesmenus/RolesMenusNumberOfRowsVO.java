package top_shopping_centre.com.vo.rolesmenus;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class RolesMenusNumberOfRowsVO {
    private Integer numberOfRows;
}
