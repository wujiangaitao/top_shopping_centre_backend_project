package top_shopping_centre.com.vo.userInformation;


import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class UserInformationListVO {
    private String userInformationName;
    private String userInformationPassword;
    private String roleName;
}
