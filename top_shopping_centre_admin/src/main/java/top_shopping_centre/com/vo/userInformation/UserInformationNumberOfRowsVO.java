package top_shopping_centre.com.vo.userInformation;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jiangfeng
 */
@AllArgsConstructor
@Data
public class UserInformationNumberOfRowsVO {
    private Integer numberOfRows;
    private String userName;
}
