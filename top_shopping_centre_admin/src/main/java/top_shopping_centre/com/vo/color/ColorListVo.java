package top_shopping_centre.com.vo.color;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.com.entity.ColorEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
@AllArgsConstructor
@Data
public class ColorListVo {
    private List<ColorEntity> colorEntities;
}
