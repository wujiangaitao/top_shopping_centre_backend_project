package top_shopping_centre.com.vo.color;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jiangfeng
 */
@AllArgsConstructor
@Data
public class ColorNumberOfRowsVO {
    private Integer numberOfRows;
}
