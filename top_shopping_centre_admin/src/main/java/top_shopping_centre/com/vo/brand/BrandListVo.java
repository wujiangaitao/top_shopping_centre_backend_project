package top_shopping_centre.com.vo.brand;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.com.entity.BrandEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
@AllArgsConstructor
@Data
public class BrandListVo {
    private List<BrandEntity> brandEntityList;
}
