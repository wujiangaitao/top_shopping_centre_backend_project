package top_shopping_centre.com.vo.brand;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author jiangfeng
 */
@AllArgsConstructor
@Data
public class BrandNumberOfRowsVo {
    private Integer numberOfRows;
}
