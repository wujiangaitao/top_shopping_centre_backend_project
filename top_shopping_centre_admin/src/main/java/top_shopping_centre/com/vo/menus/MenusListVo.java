package top_shopping_centre.com.vo.menus;

import lombok.AllArgsConstructor;
import lombok.Data;
import top_shopping_centre.com.entity.MenuEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
@Data
@AllArgsConstructor
public class MenusListVo {
    private List<MenuEntity> menuEntityList;
}
