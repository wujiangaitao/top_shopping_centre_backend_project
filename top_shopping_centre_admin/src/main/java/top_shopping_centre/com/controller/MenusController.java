package top_shopping_centre.com.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.com.service.MenusService;
import top_shopping_centre.com.vo.ResponseVO;

import static top_shopping_centre.com.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 * 菜单
 */
@RestController
@RequestMapping("/admin/menus")
@RequiredArgsConstructor
public class MenusController {
    private final MenusService menusService;

    @RequestMapping("/select/menus")
    public ResponseVO selectAllMenus(){
        return succeed(menusService.selectMenus());
    }
}
