package top_shopping_centre.com.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.com.dto.userInformation.UserDTO;
import top_shopping_centre.com.service.UserInformationService;
import top_shopping_centre.com.vo.ResponseVO;

import static top_shopping_centre.com.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 */
@RestController
@RequestMapping("/admin/user")
@RequiredArgsConstructor
public class UserInformationController {

    private final UserInformationService userInformationService;

    @RequestMapping("/login/users")
    public ResponseVO loginUserInfo(@RequestBody UserDTO userDTO){
        return succeed(userInformationService.loginUserInfo(userDTO));
    }

    @RequestMapping("/quan/list")
    public ResponseVO selectUserInfo(){
        return succeed(userInformationService.selectListAllUserInfo());
    }
}
