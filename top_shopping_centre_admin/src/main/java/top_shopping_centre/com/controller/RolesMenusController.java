package top_shopping_centre.com.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.com.dto.rolesmenus.RoleDTO;
import top_shopping_centre.com.service.RolesMenusService;
import top_shopping_centre.com.vo.ResponseVO;

import static top_shopping_centre.com.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 */
@RestController
@RequestMapping("/admin/role")
@RequiredArgsConstructor
public class RolesMenusController {

    private final RolesMenusService rolesMenusService;

    @RequestMapping("/select/menus")
    public ResponseVO selectRoleMenus(@RequestBody RoleDTO roleDTO){
        return succeed(rolesMenusService.selectRoleMenus(roleDTO));
    }
}
