package top_shopping_centre.com.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.com.dto.brand.BrandDTO;
import top_shopping_centre.com.service.BrandService;
import top_shopping_centre.com.vo.ResponseVO;

import static top_shopping_centre.com.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 */
@RestController
@RequestMapping("/admin/brand")
@RequiredArgsConstructor
public class BrandController {

    private final BrandService tableBrandService;

    @RequestMapping("/select/brand")
    public ResponseVO selectBrandList(){
        return succeed(tableBrandService.selectBrandList());
    }

    @RequestMapping("/update/brand")
    public ResponseVO updateBrand(@RequestBody BrandDTO brandDTO){
        return succeed(tableBrandService.updateBrandStatus(brandDTO));
    }

    @RequestMapping("/delete/brand")
    public ResponseVO deleteBrand(@RequestBody BrandDTO brandDTO){
        return succeed(tableBrandService.deleteBrand(brandDTO));
    }
}
