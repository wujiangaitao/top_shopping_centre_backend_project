package top_shopping_centre.com.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.com.dto.color.ColorDTO;
import top_shopping_centre.com.service.ColorService;
import top_shopping_centre.com.vo.ResponseVO;

import static top_shopping_centre.com.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 */
@RestController
@RequestMapping("/admin/color")
@RequiredArgsConstructor
public class ColorController {
    private final ColorService tableColorService;


    @RequestMapping("/list/color")
    public ResponseVO colorList(){
        return succeed(tableColorService.colorList());
    }

    @RequestMapping("/insert/color")
    public ResponseVO colorInsert(@RequestBody ColorDTO colorDTO){
        return succeed(tableColorService.colorInsert(colorDTO));
    }

    @RequestMapping("/delete/color")
    public ResponseVO colorDelete(@RequestBody ColorDTO colorDTO){
        return succeed(tableColorService.colorDelete(colorDTO));
    }
}
