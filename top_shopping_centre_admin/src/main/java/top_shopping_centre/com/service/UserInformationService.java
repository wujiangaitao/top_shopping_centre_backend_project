package top_shopping_centre.com.service;

import top_shopping_centre.com.dto.userInformation.UserDTO;
import top_shopping_centre.com.entity.UserInformationEntity;
import top_shopping_centre.com.vo.userInformation.UserInformationListVO;
import top_shopping_centre.com.vo.userInformation.UserInformationNumberOfRowsVO;

import java.util.List;

/**
 * @author jiangfeng
 */
public interface UserInformationService {

    /**
     * 登录查询
     * */
    UserInformationNumberOfRowsVO loginUserInfo(UserDTO userDTO);


    /**
     * 查询所以用户信息
     * */
    List<UserInformationListVO> selectListAllUserInfo();
}
