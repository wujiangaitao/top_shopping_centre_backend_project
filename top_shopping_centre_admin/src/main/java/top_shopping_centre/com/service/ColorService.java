package top_shopping_centre.com.service;

import top_shopping_centre.com.dto.color.ColorDTO;
import top_shopping_centre.com.vo.color.ColorNumberOfRowsVO;
import top_shopping_centre.com.vo.color.ColorListVo;

/**
 * @author jiangfeng
 */
public interface ColorService {
    /**
     * 查询所有颜色
     * */
    ColorListVo colorList();

    /**
     * 添加颜色信息
     * */

    ColorNumberOfRowsVO colorInsert(ColorDTO colorDTO);


    /**
     * 删除颜色信息
     * */
    ColorNumberOfRowsVO colorDelete(ColorDTO colorDTO);


}
