package top_shopping_centre.com.service;

import top_shopping_centre.com.vo.menus.MenusListVo;

/**
 * @author jiangfeng
 */
public interface MenusService {
    MenusListVo selectMenus();
}
