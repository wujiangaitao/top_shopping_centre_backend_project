package top_shopping_centre.com.service;

import top_shopping_centre.com.dto.brand.BrandDTO;
import top_shopping_centre.com.vo.brand.BrandListVo;
import top_shopping_centre.com.vo.brand.BrandNumberOfRowsVo;

/**
 * @author jiangfeng
 */
public interface BrandService {

    /**
     * 查询所有品牌
     * */
    BrandListVo selectBrandList();

    /**
     * 修改品牌状态
     * */
    BrandNumberOfRowsVo updateBrandStatus(BrandDTO brandDTO);

    /**
     * 删除品牌
     * */
    BrandNumberOfRowsVo deleteBrand(BrandDTO brandDTO);
}
