package top_shopping_centre.com.service;

import top_shopping_centre.com.dto.rolesmenus.RoleDTO;
import top_shopping_centre.com.vo.rolesmenus.MenuVO;

/**
 * @author jiangfeng
 */
public interface RolesMenusService {

    MenuVO selectRoleMenus(RoleDTO roleDTO);

}
