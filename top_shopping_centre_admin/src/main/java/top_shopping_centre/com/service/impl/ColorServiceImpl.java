package top_shopping_centre.com.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import top_shopping_centre.com.dto.color.ColorDTO;

import top_shopping_centre.com.entity.ColorEntity;
import top_shopping_centre.com.exception.ColorExistException;
import top_shopping_centre.com.mapper.ColorMapper;
import top_shopping_centre.com.service.ColorService;
import top_shopping_centre.com.vo.color.ColorListVo;
import top_shopping_centre.com.vo.color.ColorNumberOfRowsVO;

import java.util.Objects;


/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class ColorServiceImpl implements ColorService {

    private final ColorMapper colorMapper;

    /**
     * 查询
     * */
    @Override
    public ColorListVo colorList() {
        return new ColorListVo(colorMapper.colorList());
    }

    /**
     * 添加
     * */
    @Override
    public ColorNumberOfRowsVO colorInsert(ColorDTO colorDTO) {


        ColorEntity color = getColorDTO(colorDTO);

        exist(colorDTO, color);

        return new ColorNumberOfRowsVO(colorMapper.insertColor(color));
    }

    /**
     * 判断颜色是否存在，并且不存在，也不可以填写空值
     * */
    private void exist(ColorDTO colorDTO, ColorEntity color) {
        ColorEntity colorEntity = colorMapper.selectColorExist(color);
        if (colorEntity !=null&& Objects.equals(colorEntity.getTopColorName(),colorDTO.getColorName())){
            throw new ColorExistException("颜色已经存在，请重新选择颜色进行添加!");
        }
        else {
            if (color.getTopColorName().isEmpty()){
                throw new ColorExistException("不可以填空值");
            }
        }
    }

    /**
     * 删除
     * */
    @Override
    public ColorNumberOfRowsVO colorDelete(ColorDTO colorDTO) {

        ColorEntity color = getColorDTO(colorDTO);

        return new ColorNumberOfRowsVO(colorMapper.deleteColor(color));
    }

    /**
     * DTO转实体
     * */
    private ColorEntity getColorDTO(ColorDTO colorDTO) {
        ColorEntity colorEntity = new ColorEntity();
        colorEntity.setId(colorDTO.getColorId());
        colorEntity.setTopColorName(colorDTO.getColorName());
        return colorEntity;
    }
}
