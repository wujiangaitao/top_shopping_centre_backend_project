package top_shopping_centre.com.service.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import top_shopping_centre.com.dto.rolesmenus.RoleDTO;
import top_shopping_centre.com.entity.MenuEntity;
import top_shopping_centre.com.entity.RolesMenusEntity;
import top_shopping_centre.com.mapper.RolesMenusMapper;
import top_shopping_centre.com.service.RolesMenusService;
import top_shopping_centre.com.vo.rolesmenus.MenuVO;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class RolesMenusServiceImpl implements RolesMenusService {

    private final RolesMenusMapper rolesMenusMapper;

    @Override
    public MenuVO selectRoleMenus(RoleDTO roleDTO) {

        RolesMenusEntity rolesMenusEntity = setUp(roleDTO);

        return new MenuVO(getMenuEntityList(rolesMenusEntity));
    }

    /**
     * 查询是否为顶级，如果是顶级则查询它的子
     * */
    @NotNull
    private List<MenuEntity> getMenuEntityList(RolesMenusEntity rolesMenusEntity) {
        List<MenuEntity> menuEntities = rolesMenusMapper.selectRolesMenus(rolesMenusEntity);
        List<MenuEntity> entityArrayList = new ArrayList<>();
        for (MenuEntity menuEntity : menuEntities) {
            if (menuEntity.gettMenuFatherId() == 0) {
                entityArrayList.add(menuEntity);
                List<MenuEntity> childMenus = menuEntities.stream()
                        .filter(childMenu -> Objects.equals(childMenu.gettMenuFatherId(), menuEntity.getmId()))
                        .collect(Collectors.toList());
                menuEntity.setZi(childMenus);
            }
        }
        return entityArrayList;
    }

    private  RolesMenusEntity setUp(RoleDTO roleDTO) {

        RolesMenusEntity rolesMenusEntity=new RolesMenusEntity();

        rolesMenusEntity.setRId(roleDTO.getrId());

        return rolesMenusEntity;
    }

}
