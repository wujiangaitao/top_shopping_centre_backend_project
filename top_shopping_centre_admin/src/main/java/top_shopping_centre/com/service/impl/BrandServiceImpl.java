package top_shopping_centre.com.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import top_shopping_centre.com.dto.brand.BrandDTO;
import top_shopping_centre.com.entity.BrandEntity;
import top_shopping_centre.com.mapper.BrandMapper;
import top_shopping_centre.com.service.BrandService;
import top_shopping_centre.com.vo.brand.BrandListVo;
import top_shopping_centre.com.vo.brand.BrandNumberOfRowsVo;

/**
 * @author jiangfeng
 */
@Service
@AllArgsConstructor
public class BrandServiceImpl implements BrandService {
    private final BrandMapper brandMapper;
    @Override
    public BrandListVo selectBrandList() {
        return new BrandListVo(brandMapper.selectBrandList());
    }

    @Override
    public BrandNumberOfRowsVo updateBrandStatus(BrandDTO brandDTO) {

        BrandEntity tableBrand = getDTO(brandDTO);

        return new BrandNumberOfRowsVo(brandMapper.updateBrandStatus(tableBrand));
    }

    /**
     * 删除品牌
     * */
    @Override
    public BrandNumberOfRowsVo deleteBrand(BrandDTO brandDTO) {

        BrandEntity tableBrand = getDTO(brandDTO);

        return new BrandNumberOfRowsVo(brandMapper.deleteBrand(tableBrand));
    }

    private BrandEntity getDTO(BrandDTO brandDTO){
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setId(brandDTO.getId());
        brandEntity.setTopBrandStatus(brandDTO.getStatus());
        return brandEntity;
    }
}
