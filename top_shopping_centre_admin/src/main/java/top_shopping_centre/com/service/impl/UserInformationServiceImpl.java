package top_shopping_centre.com.service.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import top_shopping_centre.com.dto.userInformation.UserDTO;
import top_shopping_centre.com.entity.UserInformationEntity;
import top_shopping_centre.com.mapper.UserInformationMapper;
import top_shopping_centre.com.service.UserInformationService;
import top_shopping_centre.com.vo.userInformation.UserInformationListVO;
import top_shopping_centre.com.vo.userInformation.UserInformationNumberOfRowsVO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class UserInformationServiceImpl implements UserInformationService {

    private final UserInformationMapper userInformationMapper;
    @Override
    public UserInformationNumberOfRowsVO loginUserInfo(UserDTO userDTO) {

        UserInformationEntity userInformationEntity = setUp(userDTO);

        return getUserInformationNumberOfRowsVO(userInformationEntity);
    }

    @Override
    public List<UserInformationListVO> selectListAllUserInfo() {
        List<UserInformationEntity> userInformationEntities = userInformationMapper.selectListAllUserInfo();

        return getUserList(userInformationEntities);
    }

    /**
     * 获取所以信息
     * */
    @NotNull
    private static List<UserInformationListVO> getUserList(List<UserInformationEntity> userInformationEntities) {
        List<UserInformationListVO> u = new ArrayList<>();
        for (UserInformationEntity userInformationEntity : userInformationEntities) {
            UserInformationListVO userInformationListVO = new UserInformationListVO();
            userInformationListVO.setUserInformationName(userInformationEntity.getTUserInformationName());
            userInformationListVO.setUserInformationPassword(userInformationEntity.getTUserInformationPassword());
            userInformationListVO.setRoleName(userInformationEntity.getRoleEntity().getTRoleName());
            u.add(userInformationListVO);
        }
        return u;
    }

    @NotNull
    private UserInformationNumberOfRowsVO getUserInformationNumberOfRowsVO(UserInformationEntity userInformationEntity) {
        UserInformationEntity userInformationEntity1 = userInformationMapper.loginUserInfo(userInformationEntity);
        return new UserInformationNumberOfRowsVO(userInformationEntity1.getTRoleId()
                , userInformationEntity1.getTUserInformationName());
    }

    /**
     * DTO转实体
     * */
    public UserInformationEntity setUp(UserDTO userDTO){
        UserInformationEntity userInformationEntity = new UserInformationEntity();
        BeanUtils.copyProperties(userDTO,userInformationEntity);
        return userInformationEntity;
    }
}
