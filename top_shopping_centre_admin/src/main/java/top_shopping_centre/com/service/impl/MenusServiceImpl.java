package top_shopping_centre.com.service.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import top_shopping_centre.com.entity.MenuEntity;
import top_shopping_centre.com.mapper.MenusMapper;
import top_shopping_centre.com.service.MenusService;
import top_shopping_centre.com.vo.menus.MenusListVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class MenusServiceImpl implements MenusService {

    private final MenusMapper menusMapper;

    /**
     * 查询所有菜单的并且有子的也添加到
     * */
    @Override
    public MenusListVo selectMenus() {
        return new MenusListVo(getMenuEntities(menusMapper.selectAllMenu()));
    }

    /**
     * 查询所有的菜单，并且循环到当前菜单如果有子，就添加到父上
     * */
    @NotNull
    private static List<MenuEntity> getMenuEntities(List<MenuEntity> menuEntities) {
        List<MenuEntity> entityArrayList = new ArrayList<>();
        for (MenuEntity menuEntity : menuEntities) {
            if (menuEntity.gettMenuFatherId() == 0) {
                entityArrayList.add(menuEntity);
                List<MenuEntity> childMenus = menuEntities.stream()
                        .filter(childMenu -> Objects.equals(childMenu.gettMenuFatherId(), menuEntity.getmId()))
                        .collect(Collectors.toList());
                menuEntity.setZi(childMenus);
            }
        }
        return entityArrayList;
    }
}
