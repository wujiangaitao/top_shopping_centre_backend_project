package top_shopping_centre.com.exception;

import lombok.Getter;

/**
 * @author jiangfeng
 */
@Getter
public class ColorExistException extends RuntimeException {
    private final String msg;
    public ColorExistException(String msg) {
        this.msg=msg;
    }
}
