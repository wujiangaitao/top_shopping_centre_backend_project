package top_shopping_centre.com.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import top_shopping_centre.com.vo.ResponseVO;

import static top_shopping_centre.com.vo.EncapsulationResponseVO.fail;


/**
 * @author jiangfeng
 * 全局异常处理
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 颜色存在异常
     * */
    @ExceptionHandler(ColorExistException.class)
    public ResponseVO userLoginException(ColorExistException colorExistException){
        return fail(colorExistException.getMsg());
    }
}
