package top_shopping_centre.entity.product.image;

import lombok.Data;

/**
 * @author jiangfeng
 * 商品图片
 */
@Data
public class ProductImageEntity {
    private Integer piId;
    private Integer topProductId;
    private String topProductImageName;
}
