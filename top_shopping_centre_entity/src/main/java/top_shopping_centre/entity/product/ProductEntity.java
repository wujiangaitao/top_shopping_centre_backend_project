package top_shopping_centre.entity.product;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author jiangfeng
 * 商品实体类
 */
@Data
public class ProductEntity {
    private Integer pid;
    private String topProductName;
    private BigDecimal topProductPrice;
    private String topProductImage;
    private String topProductDesc;
    private Integer topProductStatus;
    private Integer feTopProductTypeId;
    private Integer feTopBrandId;
}
