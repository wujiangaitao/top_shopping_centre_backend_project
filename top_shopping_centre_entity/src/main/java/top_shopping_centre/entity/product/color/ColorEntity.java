package top_shopping_centre.entity.product.color;


import lombok.Data;

/**
 * @author jiangfeng
 * 颜色实体
 */
@Data
public class ColorEntity {
    private Integer id;
    private String topColorName;
}
