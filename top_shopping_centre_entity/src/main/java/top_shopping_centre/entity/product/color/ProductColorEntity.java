package top_shopping_centre.entity.product.color;

import lombok.Data;

/**
 * @author jiangfeng
 * 商品颜色实体
 */
@Data
public class ProductColorEntity {
    private Integer pCid;
    private Integer topProductId;
    private Integer topColorId;
}
