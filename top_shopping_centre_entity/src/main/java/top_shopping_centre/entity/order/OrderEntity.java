package top_shopping_centre.entity.order;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author jiangfeng
 */
@Data
public class OrderEntity {
    private Integer oId;
    private Integer feTopCatId;
    private Integer topOrderStatus;
    private String topOrderProductName;
    private String topOrderProductSpecification;
    private Integer topOrderQuantity;
    private LocalDateTime topOrderDate;
    private BigDecimal topOrderPrice;
    private Integer topOrderUserId;
    private String topOrderUserProvince;
    private String topOrderUserCity;
    private String topOrderUserCounty;
    private String topOrderUserDetailed;
    private String topOrderUserName;
    private String topOrderUserPhone;
}
