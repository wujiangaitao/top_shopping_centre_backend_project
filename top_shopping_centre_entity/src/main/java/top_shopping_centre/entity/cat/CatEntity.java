package top_shopping_centre.entity.cat;

import lombok.Data;
import top_shopping_centre.entity.product.ProductEntity;

import java.util.List;

/**
 * @author jiangfeng
 */
@Data
public class CatEntity {
    private Integer cid;
    private Integer feTopProductId;
    private Integer topCatProductQuantity;
    private String topCatProductSpecification;
    private List<ProductEntity> productEntities;
    private Integer topCatProductStatus;
    private Integer topCatUserId;
}
