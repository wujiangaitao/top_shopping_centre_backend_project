package top_shopping_centre.entity.user;

import lombok.Data;
import top_shopping_centre.entity.user.userlogin.UserLoginEntity;

import java.time.LocalDateTime;

/**
 * @author jiangfeng
 */
@Data
public class UserEntity {
    private Integer id;
    private String userRealName;
    private String userIdentityCard;
    private String userImage;
    private Character userSex;
    private String userPhone;
    private LocalDateTime gmtUserData;
    private Integer feUserLoginId;
    private UserLoginEntity userLoginEntity;
}
