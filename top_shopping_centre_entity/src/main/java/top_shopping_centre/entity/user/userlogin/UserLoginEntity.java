package top_shopping_centre.entity.user.userlogin;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author jiangfeng
 */
@Data
public class UserLoginEntity {
    private Integer ulId;
    private String userLoginName;
    private String userLoginPwd;
    private String userLoginStatus;
    private LocalDateTime gmtUserLoginData;
}
