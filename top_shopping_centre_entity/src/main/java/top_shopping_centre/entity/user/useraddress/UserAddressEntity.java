package top_shopping_centre.entity.user.useraddress;

import lombok.Data;

/**
 * @author jiangfeng
 */
@Data
public class UserAddressEntity {
    private Integer id;
    private String userAddressProvince;
    private String userAddressCity;
    private String userAddressCounty;
    private String userAddressDetailed;
    private String userAddressName;
    private String userAddressPhone;
    private Integer userAddressDefault;
    private Integer feUserId;
}
