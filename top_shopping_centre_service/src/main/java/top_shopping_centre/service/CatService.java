package top_shopping_centre.service;

import top_shopping_centre.dto.cat.CatDTO;
import top_shopping_centre.vo.cat.CatListVO;
import top_shopping_centre.vo.cat.CatNumberOfRowsVO;
/**
 * @author jiangfeng
 */
public interface CatService {

    /**
     * 添加购物车
     * */
    CatNumberOfRowsVO insertCat(CatDTO catDTO);

    /**
     * 查询购物车所有商品
     * */
    CatListVO selectCatAllProduct(CatDTO catDTO);

    /**
     * 删除购物车
     */
    CatNumberOfRowsVO deleteCatProduct(CatDTO catDTO);

    /**
     * 修改购物车数量:--
     * */
    CatNumberOfRowsVO updateCatProductJan(CatDTO catDTO);


}
