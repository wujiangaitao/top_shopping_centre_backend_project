package top_shopping_centre.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top_shopping_centre.dto.product.ProductDTO;
import top_shopping_centre.entity.product.color.ColorEntity;
import top_shopping_centre.entity.product.image.ProductImageEntity;
import top_shopping_centre.mapper.ProductMapper;
import top_shopping_centre.entity.product.ProductEntity;
import top_shopping_centre.service.ProductService;
import top_shopping_centre.vo.product.ColorVO;
import top_shopping_centre.vo.product.ImageListVO;
import top_shopping_centre.vo.product.ProductListVO;
import top_shopping_centre.vo.product.ProductVO;

import java.util.List;

/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductMapper productMapper;

    /**
     * 查询所有商品
     * */
    @Override
    public ProductListVO getAllProduct() {
        List<ProductEntity> allProduct = productMapper.getAllProduct();


        return new ProductListVO(allProduct);
    }

    /**
     * 通过商品Id查询当前商品所有的信息
     * */
    @Override
    public ProductVO getProductById(ProductDTO productDTO) {
        ProductEntity productEntity = getProductEntity(productDTO);

        ProductEntity productById = productMapper.getProductById(productEntity);


        return new ProductVO(productById);
    }


    /**
     * 查询当前商品的颜色
     * */
    @Override
    public ColorVO getProductColor(ProductDTO productDTO) {
        ProductEntity productEntity = getProductEntity(productDTO);

        List<ColorEntity> productColor = productMapper.getProductColor(productEntity);


        return new ColorVO(productColor);
    }

    /**
     * 查询当前商品的图片
     * */
    @Override
    public ImageListVO getProductAllImage(ProductDTO productDTO) {
        ProductEntity productEntity = getProductEntity(productDTO);

        List<ProductImageEntity> productAllImage = productMapper.getProductAllImage(productEntity);


        return new ImageListVO(productAllImage);
    }



    /**
     * DTO转为实体
     * */
    private ProductEntity getProductEntity(ProductDTO productDTO) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setPid(productDTO.getId());
        return productEntity;
    }

}
