package top_shopping_centre.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top_shopping_centre.dto.user.UserLoginDTO;
import top_shopping_centre.entity.user.UserEntity;
import top_shopping_centre.entity.user.userlogin.UserLoginEntity;
import top_shopping_centre.mapper.UserLoginMapper;
import top_shopping_centre.mapper.UserMapper;
import top_shopping_centre.service.UserLoginService;
import top_shopping_centre.util.JwtUtils;
import top_shopping_centre.vo.user.UserLoginNumberOfRowsVO;


import java.time.LocalDateTime;

/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class UserLoginServiceImpl implements UserLoginService {

    private final UserMapper userMapper;

    private final UserLoginMapper userLoginMapper;

    /**
     * 登录或者或者是注册
     * */
    @Override
    public UserLoginNumberOfRowsVO loginOrRegister(UserLoginDTO userLoginDTO) {

        UserLoginEntity userLoginEntity = setEnTitty(userLoginDTO);

        UserLoginNumberOfRowsVO number = getUserLoginNumberOfRowsVO(userLoginDTO, userLoginEntity);
        if (number != null) {
            return number;
        }
        return null;
    }

    /**
     * 如果存在就是登录，如果不存在就是注册，每一次登录会先查询是否存在当前用户
     * */
    private UserLoginNumberOfRowsVO getUserLoginNumberOfRowsVO(UserLoginDTO userLoginDTO, UserLoginEntity userLoginEntity) {
        UserLoginEntity userLogin = userLoginMapper.selectUserExist(userLoginEntity);
        UserEntity userEntity=new UserEntity();
        String jwt = JwtUtils.createJwt();
        if (userLogin==null){
            userLoginMapper.insertUserLoginInformation(userLoginEntity);

            userInsert(userLoginEntity,userEntity);

            return new UserLoginNumberOfRowsVO(userEntity.getId(),jwt);
        }
        else if (userLogin.getUserLoginName().equals(userLoginDTO.getUserLoginName())&&userLogin.getUserLoginPwd().equals(userLoginDTO.getUserLoginPwd())){
            userEntity.setId(userLogin.getUlId());
            return new UserLoginNumberOfRowsVO(userEntity.getId(),jwt);
        }
        return null;
    }

    /**
     * 给当前登录用户创建一个用户信息
     * */
    private void userInsert(UserLoginEntity userLoginEntity,UserEntity userEntity) {
        userEntity.setFeUserLoginId(userLoginEntity.getUlId());
        userMapper.insertUserInformation(userEntity);
    }


    /**
     * DTO转为实体
     * */
    private  UserLoginEntity setEnTitty(UserLoginDTO userLoginDTO)  {
        UserLoginEntity userLoginEntity = new UserLoginEntity();
        userLoginEntity.setUserLoginName(userLoginDTO.getUserLoginName());
        userLoginEntity.setUserLoginPwd(userLoginDTO.getUserLoginPwd());
        userLoginEntity.setUserLoginStatus(userLoginDTO.getUserLoginStatus());
        userLoginEntity.setGmtUserLoginData(LocalDateTime.now());
        return userLoginEntity;
    }

}
