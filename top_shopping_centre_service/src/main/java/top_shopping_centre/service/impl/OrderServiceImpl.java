package top_shopping_centre.service.impl;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import top_shopping_centre.dto.order.OrderDTO;
import top_shopping_centre.dto.pay.PayProductDTO;
import top_shopping_centre.entity.cat.CatEntity;
import top_shopping_centre.entity.order.OrderEntity;
import top_shopping_centre.mapper.CatMapper;
import top_shopping_centre.mapper.OrderMapper;
import top_shopping_centre.service.OrderService;
import top_shopping_centre.vo.order.OrderListVO;
import top_shopping_centre.vo.order.OrderNumberOfRowsVO;
import top_shopping_centre.vo.order.OrderVO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderMapper orderMapper;

    private final CatMapper catMapper;


    /**
     * 添加到订单表中，并且修改了购物车的状态，购物车状态改为9，等于正在支付，并且订单表中的状态为0，等待支付
     * */
    @Override
    public OrderNumberOfRowsVO insertOrder(OrderDTO orderDTO) {
        orderSet(orderDTO);

        return new OrderNumberOfRowsVO(orderMapper.insertOrder(getOrderEntities(orderDTO)));
    }


    /**
     * 查询订单表中所有等支付的订单
     * */
    @Override
    public OrderListVO userOrderProduct(OrderDTO orderDTO) {

        OrderEntity orderEntity = getOrderEntityE(orderDTO);

        return new OrderListVO(orderMapper.userOrderProduct(orderEntity));
    }

    /**
     * DTO转为实体:OrderEntity没有主id
     * */
    @NotNull
    private  OrderEntity getOrderEntityE(OrderDTO orderDTO) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOId(orderDTO.getId());
        orderEntity.setTopOrderStatus(orderDTO.getStatus());
        orderEntity.setTopOrderUserId(orderDTO.getUserId());
        return orderEntity;
    }

    /**
     * 支付成功的信息，修改订单表的状态为支付成功，并且购物车的状态也修改为已支付
     * */
    @Override
    public OrderNumberOfRowsVO updateOrder(OrderDTO orderDTO) {
        List<CatEntity> catEntityList=new ArrayList<>();
        for (Integer i : orderDTO.getCatId()) {
            CatEntity catEntity = getCatEntity(orderDTO, i);
            catEntityList.add(catEntity);
        }

        List<OrderEntity> orderEntityList=new ArrayList<>();
        for (Integer i : orderDTO.getOrderId()) {
            OrderEntity orderEntity = getOrderEntity(orderDTO, i);
            orderEntityList.add(orderEntity);
        }

        catMapper.updateCatStatus(catEntityList);
        return new OrderNumberOfRowsVO(orderMapper.updateOrderStatus(orderEntityList));
    }

    /**
     * 查询当前用户的所有订单
     * */
    @Override
    public OrderListVO selectUserOrderListAndUserId(OrderDTO orderDTO) {

        OrderEntity orderEntity = getOrderEntityE(orderDTO);

        return new OrderListVO(orderMapper.selectUserOrderListAndUserId(orderEntity));
    }

    /**
     * 取消支付
     * */
    @Override
    public OrderNumberOfRowsVO userCancelThePaymentOrder(OrderDTO orderDTO) {

        OrderEntity orderEntity = getOrderEntityE(orderDTO);

        return new OrderNumberOfRowsVO(orderMapper.userCancelThePaymentOrder(orderEntity));
    }

    /**
     * 用户传入id单个订单信息
     * */
    @Override
    public OrderVO userOrderIdInfo(OrderDTO orderDTO) {

        OrderEntity orderEntityE = getOrderEntityE(orderDTO);

        return new OrderVO(orderMapper.userOrderIdInfo(orderEntityE));
    }


    /**
     * DTO转为实体：OrderEntity,有主id
     * */
    @NotNull
    private  OrderEntity getOrderEntity(OrderDTO orderDTO, Integer i) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOId(i);
        orderEntity.setTopOrderStatus(orderDTO.getStatus());
        orderEntity.setTopOrderUserId(orderDTO.getUserId());
        orderEntity.setTopOrderUserProvince(orderDTO.getTopOrderUserProvince());
        orderEntity.setTopOrderUserCity(orderDTO.getTopOrderUserCity());
        orderEntity.setTopOrderUserCounty(orderDTO.getTopOrderUserCounty());
        orderEntity.setTopOrderUserDetailed(orderDTO.getTopOrderUserDetailed());
        orderEntity.setTopOrderUserName(orderDTO.getTopOrderUserName());
        orderEntity.setTopOrderUserPhone(orderDTO.getTopOrderUserPhone());
        return orderEntity;
    }


    /**
     * DTO转为实体：CatEntity
     * */
    @NotNull
    private  CatEntity getCatEntity(OrderDTO orderDTO, Integer i) {
        CatEntity catEntity = new CatEntity();
        catEntity.setCid(i);
        catEntity.setTopCatProductStatus(orderDTO.getStatus());
        catEntity.setTopCatUserId(orderDTO.getUserId());
        return catEntity;
    }

    /**
     * 把信息添加到订单表里面
     * */
    @NotNull
    private static List<OrderEntity> getOrderEntities(OrderDTO orderDTO) {
        List<OrderEntity> orderEntityList=new ArrayList<>();
        for (PayProductDTO payProductDTO : orderDTO.getProductDTOList()) {
            OrderEntity orderEntity = new OrderEntity();
            orderEntity.setFeTopCatId(payProductDTO.getCatId());
            orderEntity.setTopOrderProductName(payProductDTO.getTopProductName());
            orderEntity.setTopOrderProductSpecification(payProductDTO.getTopProductDesc());
            orderEntity.setTopOrderStatus(payProductDTO.getStatus());
            orderEntity.setTopOrderQuantity(payProductDTO.getQuantity());
            orderEntity.setTopOrderPrice(payProductDTO.getTopProductPrice());
            orderEntity.setTopOrderDate(LocalDateTime.now());
            orderEntity.setTopOrderUserId(payProductDTO.getUserId());
            orderEntityList.add(orderEntity);
        }
        return orderEntityList;
    }

    /**
     * 设置购物车状态
     * */
    private void orderSet(OrderDTO orderDTO) {
        List<CatEntity> catEntityList=new ArrayList<>();
        for (Integer i : orderDTO.getCatId()) {
            CatEntity cat = new CatEntity();
            cat.setCid(i);
            cat.setTopCatProductStatus(orderDTO.getCatStatus());
            cat.setTopCatUserId(orderDTO.getUserId());
            catEntityList.add(cat);
        }
        catMapper.updateCatStatus(catEntityList);
    }

}
