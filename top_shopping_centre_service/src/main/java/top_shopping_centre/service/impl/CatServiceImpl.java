package top_shopping_centre.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top_shopping_centre.dto.cat.CatDTO;
import top_shopping_centre.entity.cat.CatEntity;
import top_shopping_centre.entity.product.ProductEntity;
import top_shopping_centre.mapper.CatMapper;
import top_shopping_centre.mapper.ProductMapper;
import top_shopping_centre.service.CatService;
import top_shopping_centre.vo.cat.CatListVO;
import top_shopping_centre.vo.cat.CatNumberOfRowsVO;
import java.util.List;

/**
 * @author jiangfeng
 */
@Service
@RequiredArgsConstructor
public class CatServiceImpl implements CatService {

    private final CatMapper catMapper;

    private final ProductMapper productMapper;

    /**
     * 添加购物车，先查询，如果存在就数量++，不存在就添加
     * */
    @Override
    public CatNumberOfRowsVO insertCat(CatDTO catDTO) {
        CatEntity catEntity = getCatEntity(catDTO);

        List<CatEntity> catEntities = catMapper.selectCatProduct(catEntity);
        if (catEntities!=null){
            for (CatEntity entity : catEntities) {
                if (entity.getFeTopProductId().equals(catEntity.getFeTopProductId())
                        &&entity.getTopCatProductSpecification().
                        equals(catEntity.getTopCatProductSpecification())&&entity.getTopCatUserId()
                        .equals(catEntity.getTopCatUserId())&&entity.getTopCatProductStatus().
                        equals(catEntity.getTopCatProductStatus())){
                    return new CatNumberOfRowsVO(catMapper.updateCatProductJia(catEntity));
                }
            }
        }
        return getCatNumberOfRowsVO(catEntity);
    }

    /**
     * 添加进去购物车
     * */
    private CatNumberOfRowsVO getCatNumberOfRowsVO(CatEntity catEntity) {
        return new CatNumberOfRowsVO(catMapper.insertCat(catEntity));
    }


    /**
     * 查询购物车所有的商品
     * */
    @Override
    public CatListVO selectCatAllProduct(CatDTO catDTO) {
        CatEntity cat = getCatEntity(catDTO);
        List<CatEntity> catEntities = catMapper.selectCatAllProduct(cat);

        for (CatEntity catEntity : catEntities) {
            List<ProductEntity> productEntities = productMapper.selectProductId(catEntity.getFeTopProductId());
            catEntity.setProductEntities(productEntities);
        }

        return new CatListVO(catEntities);
    }

    /**
     * 删除购物车信息
     * */
    @Override
    public CatNumberOfRowsVO deleteCatProduct(CatDTO catDTO) {

        CatEntity catEntity = getCatEntity(catDTO);

        return new CatNumberOfRowsVO(catMapper.deleteCatProduct(catEntity));
    }


    /**
     * 修改购物车数量--
     * */
    @Override
    public CatNumberOfRowsVO updateCatProductJan(CatDTO catDTO) {
        CatEntity catEntity = getCatEntity(catDTO);

        List<CatEntity> catEntities = catMapper.selectCatProduct(catEntity);
        if (catEntities!=null){
            for (CatEntity entity : catEntities) {
                if (entity.getFeTopProductId().equals(catEntity.getFeTopProductId())
                        &&entity.getTopCatProductSpecification().
                        equals(catEntity.getTopCatProductSpecification())&&entity.
                        getTopCatUserId().equals(catEntity.getTopCatUserId())&&entity.getTopCatProductStatus().
                        equals(catEntity.getTopCatProductStatus())){
                    return new CatNumberOfRowsVO(catMapper.updateCatProductJan(catEntity));
                }
            }
        }

        return new CatNumberOfRowsVO(null);
    }


    /**
     * DTO转实体
     * */
    private CatEntity getCatEntity(CatDTO catDTO) {
        CatEntity catEntity = new CatEntity();
        catEntity.setCid(catDTO.getCatId());
        catEntity.setFeTopProductId(catDTO.getFeTopProductId());
        catEntity.setTopCatProductSpecification(catDTO.getTopCatProductSpecification());
        catEntity.setTopCatProductQuantity(catDTO.getTopCatProductQuantity());
        catEntity.setTopCatProductStatus(catDTO.getTopCatProductStatus());
        catEntity.setTopCatUserId(catDTO.getTopCatUserId());
        return catEntity;
    }


}
