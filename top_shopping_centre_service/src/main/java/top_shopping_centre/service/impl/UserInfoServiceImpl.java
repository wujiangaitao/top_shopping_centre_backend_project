package top_shopping_centre.service.impl;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import top_shopping_centre.dto.user.UserInfoDTO;
import top_shopping_centre.entity.user.UserEntity;
import top_shopping_centre.entity.user.userlogin.UserLoginEntity;
import top_shopping_centre.mapper.UserLoginMapper;
import top_shopping_centre.mapper.UserMapper;
import top_shopping_centre.service.UserInfoService;
import top_shopping_centre.vo.user.UserInfoNumberOfRowsVO;
import top_shopping_centre.vo.user.UserInfoVO;
import java.io.InputStream;
import java.time.LocalDateTime;

import static top_shopping_centre.util.Constants.minio.USER_BUCKET;

/**
 * @author jiangfeng
 */
@Service
@AllArgsConstructor
public class UserInfoServiceImpl implements UserInfoService {

    private final UserMapper userMapper;

    private final MinioClient minioClient;

    private final UserLoginMapper userLoginMapper;

    /**
     * 查询用户信息，通过用户id
     * */
    @Override
    public UserInfoVO selectUserInformation(UserInfoDTO userInfoDTO) {

        UserEntity userEntity = getUserEntity(userInfoDTO);

        return new UserInfoVO(userEntity);
    }

    /**
     * 把用户信息修改完整
     * */
    @Override
    public UserInfoNumberOfRowsVO updateUserInfo(UserInfoDTO userInfoDTO){

        String userImgName = minioPutUserImg(userInfoDTO);

        UserEntity userEntity = setup(userInfoDTO, userImgName);

        setUserLoginName(userInfoDTO);

        return new UserInfoNumberOfRowsVO(userMapper.updateUserInfo(userEntity));
    }

    /**
     * 修改用户名字
     * */
    private void setUserLoginName(UserInfoDTO userInfoDTO) {
        UserLoginEntity userLoginEntity = new UserLoginEntity();
        userLoginEntity.setUserLoginName(userInfoDTO.getUsername());
        userLoginEntity.setUlId(userInfoDTO.getUserId());
        userLoginMapper.updateUserLoginInformation(userLoginEntity);
    }


    /**
     * 把用户的图片上传到minio并且把图片名修改为用户的登录名称
     * */
    private String minioPutUserImg(UserInfoDTO userInfoDTO) {
        String uName;
        try {
           if (userInfoDTO.getFile()==null){
               return null;
           }
            //获得文件
            MultipartFile file = userInfoDTO.getFile();
            //获取文件名字
            String originalFilename = file.getOriginalFilename();
            assert originalFilename != null;
            //拿到文件后缀
            int i = originalFilename.lastIndexOf(".");
            //提取文件后缀
            String substring = originalFilename.substring(i + 1);
            //重新命名
            uName = userInfoDTO.getUsername()+"."+substring;
            //获取文件流
            InputStream inputStream = file.getInputStream();
            //开始上传
            minioClient.putObject(PutObjectArgs.builder()
                    .bucket(USER_BUCKET)
                    .object(uName)
                    .stream(inputStream,inputStream.available(),-1).build());
            inputStream.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return uName;
    }

    /**
     * 把DTO设置为实体
     * */
    private static UserEntity setup(UserInfoDTO userInfoDTO,String uNameImg) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUserRealName(userInfoDTO.getUname());
        userEntity.setUserImage(uNameImg);
        userEntity.setUserSex(userInfoDTO.getGender());
        userEntity.setUserIdentityCard(userInfoDTO.getIdentityCard());
        userEntity.setUserPhone(userInfoDTO.getPhone());
        userEntity.setGmtUserData(LocalDateTime.now());
        userEntity.setId(userInfoDTO.getUserId());
        return userEntity;
    }

    /**
     * 查询用户的所有信息并且设置了他登录账号
     * */
    private UserEntity getUserEntity(UserInfoDTO userInfoDTO) {
        UserEntity userEntity=new UserEntity();
        userEntity.setId(userInfoDTO.getUserId());
        UserEntity userEntity1 = userMapper.selectUserInformation(userEntity);
        UserLoginEntity userLoginEntity = selectUserLoginInfo(userEntity1);
        userEntity1.setUserLoginEntity(userLoginEntity);
        return userEntity1;
    }

    /**
     * 通过id查询当前用户登录的信息
     * */
    private UserLoginEntity selectUserLoginInfo(UserEntity userEntity) {
        UserLoginEntity userLoginEntity = new UserLoginEntity();
        userLoginEntity.setUlId(userEntity.getFeUserLoginId());
        return userMapper.selectUserLoginInfo(userLoginEntity);
    }
}
