package top_shopping_centre.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import top_shopping_centre.dto.user.UserAddressDTO;
import top_shopping_centre.entity.user.useraddress.UserAddressEntity;
import top_shopping_centre.mapper.UserAddressMapper;
import top_shopping_centre.service.UserAddressService;
import top_shopping_centre.vo.user.UserAddressListVO;
import top_shopping_centre.vo.user.UserAddressVO;
import top_shopping_centre.vo.user.UserInfoNumberOfRowsVO;

/**
 * @author jiangfeng
 */
@Service
@AllArgsConstructor
public class UserAddressServiceImpl implements UserAddressService {

    private final UserAddressMapper userAddressMapper;

    /**
     * 添加用户地址
     * */
    @Override
    public UserInfoNumberOfRowsVO insertUserAddress(UserAddressDTO userAddressDTO) {

        UserAddressEntity userAddressEntity = setUp(userAddressDTO);

        return new UserInfoNumberOfRowsVO(userAddressMapper.insertUserAddress(userAddressEntity));
    }

    /**
     * 查询用户地址，通过用户id
     * */
    @Override
    public UserAddressListVO selectUserAddress(UserAddressDTO userAddressDTO) {

        UserAddressEntity userAddressEntity = setUp(userAddressDTO);

        return new UserAddressListVO(userAddressMapper.selectUserAddressList(userAddressEntity));
    }

    /**
     * 删除用户地址
     * */
    @Override
    public UserInfoNumberOfRowsVO deleteUserAddress(UserAddressDTO userAddressDTO) {

        UserAddressEntity userAddressEntity = setUp(userAddressDTO);

        return new UserInfoNumberOfRowsVO(userAddressMapper.deleteUserAddress(userAddressEntity));
    }

    /**
     * 设置用户默认地址,如果之前设置了就把之前的取消，用最新设置的
     * */
    @Override
    public UserInfoNumberOfRowsVO setTheDefault(UserAddressDTO userAddressDTO) {

        UserAddressEntity userAddressEntity = setUp(userAddressDTO);

        UserAddressEntity addressEntity = userAddressMapper.selectUserAddressDefault(userAddressEntity);
        if (addressEntity==null){
            return new UserInfoNumberOfRowsVO(userAddressMapper.setTheDefault(userAddressEntity));
        }

        addressEntity.setUserAddressDefault(userAddressDTO.getUserAddressCancel());
        userAddressMapper.setTheDefault(addressEntity);
        return new UserInfoNumberOfRowsVO(userAddressMapper.setTheDefault(userAddressEntity));
    }

    /**
     * 查询用户默认地址
     * */
    @Override
    public UserAddressVO selectUserDefaultAddress(UserAddressDTO userAddressDTO) {
        UserAddressEntity userAddressEntity = setUp(userAddressDTO);
        return new UserAddressVO(userAddressMapper.selectUserAddressDefault(userAddressEntity));
    }

    /**
     * DTO设置为实体
     * */
    private UserAddressEntity setUp(UserAddressDTO userAddressDTO) {
        UserAddressEntity userAddressEntity = new UserAddressEntity();
        userAddressEntity.setId(userAddressDTO.getId());
        userAddressEntity.setUserAddressProvince(userAddressDTO.getUserAddressProvince());
        userAddressEntity.setUserAddressCity(userAddressDTO.getUserAddressCity());
        userAddressEntity.setUserAddressCounty(userAddressDTO.getUserAddressCounty());
        userAddressEntity.setUserAddressDetailed(userAddressDTO.getUserAddressDetailed());
        userAddressEntity.setUserAddressName(userAddressDTO.getUserAddressName());
        userAddressEntity.setUserAddressPhone(userAddressDTO.getUserAddressPhone());
        userAddressEntity.setUserAddressDefault(userAddressDTO.getUserAddressDefault());
        userAddressEntity.setFeUserId(userAddressDTO.getFeUserId());
        return userAddressEntity;
    }

}
