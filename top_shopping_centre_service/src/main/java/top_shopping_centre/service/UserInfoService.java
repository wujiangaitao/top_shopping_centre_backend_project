package top_shopping_centre.service;



import top_shopping_centre.dto.user.UserInfoDTO;
import top_shopping_centre.vo.user.UserInfoNumberOfRowsVO;
import top_shopping_centre.vo.user.UserInfoVO;

/**
 * @author jiangfeng
 */
public interface UserInfoService {
    /**
     * 查询用户信息
     * */
    UserInfoVO selectUserInformation(UserInfoDTO userInfoDTO);

    /**
     * 完整信息
     * */
    UserInfoNumberOfRowsVO updateUserInfo(UserInfoDTO userInfoDTO);

}
