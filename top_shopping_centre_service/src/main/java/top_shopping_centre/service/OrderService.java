package top_shopping_centre.service;

import top_shopping_centre.dto.order.OrderDTO;
import top_shopping_centre.vo.order.OrderListVO;
import top_shopping_centre.vo.order.OrderNumberOfRowsVO;
import top_shopping_centre.vo.order.OrderVO;

/**
 * @author jiangfeng
 */
public interface OrderService {
    /**
     * 添加到订单表中
     * */
    OrderNumberOfRowsVO insertOrder(OrderDTO orderDTO);

    /**
     * 查询所有预支付的订单
     * */
    OrderListVO userOrderProduct(OrderDTO orderDTO);

    /**
     * 修改支付购物车状态
     * */
    OrderNumberOfRowsVO updateOrder(OrderDTO orderDTO);


    /**
     * 查询用户当前的所有订单
     * */
    OrderListVO selectUserOrderListAndUserId(OrderDTO orderDTO);

    /**
     * 取消支付
     * */
    OrderNumberOfRowsVO userCancelThePaymentOrder(OrderDTO orderDTO);

    /**
     * 用户传订单id单个信息
     * */
    OrderVO userOrderIdInfo(OrderDTO orderDTO);

}
