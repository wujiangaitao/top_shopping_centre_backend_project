package top_shopping_centre.service;

import top_shopping_centre.dto.product.ProductDTO;
import top_shopping_centre.vo.product.ColorVO;
import top_shopping_centre.vo.product.ImageListVO;
import top_shopping_centre.vo.product.ProductListVO;
import top_shopping_centre.vo.product.ProductVO;

/**
 * @author jiangfeng
 */
public interface ProductService {
    /**
     * getAllProduct:查询所有商品
     * */
    ProductListVO getAllProduct();

    /**
     * getProductById:通过id查询单个商品信息
     * */
    ProductVO getProductById(ProductDTO productDTO);

    /**
     * getProductColor:通过商品id查询该商品的颜色
     * */
    ColorVO getProductColor(ProductDTO productDTO);

    /**
     * getProductAllImage:通过商品id查询该商品的所有图片
     * */
    ImageListVO getProductAllImage(ProductDTO productDTO);

}
