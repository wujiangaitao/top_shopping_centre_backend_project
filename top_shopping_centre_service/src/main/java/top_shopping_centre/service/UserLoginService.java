package top_shopping_centre.service;

import top_shopping_centre.dto.user.UserLoginDTO;
import top_shopping_centre.vo.user.UserLoginNumberOfRowsVO;


/**
 * @author jiangfeng
 */
public interface UserLoginService {
    /**
     * 添加登录信息
     * */
    UserLoginNumberOfRowsVO loginOrRegister(UserLoginDTO userLoginDTO);



}
