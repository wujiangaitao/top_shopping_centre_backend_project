package top_shopping_centre.service;

import top_shopping_centre.dto.user.UserAddressDTO;
import top_shopping_centre.vo.user.UserAddressListVO;
import top_shopping_centre.vo.user.UserAddressVO;
import top_shopping_centre.vo.user.UserInfoNumberOfRowsVO;

/**
 * @author jiangfeng
 */
public interface UserAddressService {

    /**
     * 添加用户地址
     * */
    UserInfoNumberOfRowsVO insertUserAddress(UserAddressDTO userAddressDTO);


    /**
     * 查询用户地址通过用户id
     * */
    UserAddressListVO selectUserAddress(UserAddressDTO userAddressDTO);


    /**
     * 删除地址通过用户id
     * */
    UserInfoNumberOfRowsVO deleteUserAddress(UserAddressDTO userAddressDTO);

    /**
     * 用户设置为默认地址
     * */
    UserInfoNumberOfRowsVO setTheDefault(UserAddressDTO userAddressDTO);


    /**
     * 查询用户默认地址
     * */
    UserAddressVO selectUserDefaultAddress(UserAddressDTO userAddressDTO);
}
