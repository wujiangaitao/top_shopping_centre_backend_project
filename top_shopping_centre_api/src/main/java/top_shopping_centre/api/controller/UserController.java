package top_shopping_centre.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.dto.user.UserInfoDTO;
import top_shopping_centre.service.UserInfoService;
import top_shopping_centre.util.ComparisonUtils;
import top_shopping_centre.vo.ResponseVO;
import javax.validation.Valid;
import static top_shopping_centre.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 */

@RestController
@RequestMapping("/userinfo")
@RequiredArgsConstructor
public class UserController {
    private final UserInfoService userInfoService;

    @RequestMapping("/select/user")
    public ResponseVO selectUserInfo(@RequestBody UserInfoDTO userInfoDTO){
       return succeed(userInfoService.selectUserInformation(userInfoDTO));
    }

    @RequestMapping("/update/user")
    public ResponseVO updateUserInfo(@Valid UserInfoDTO userInfoDTO, BindingResult result){
        ComparisonUtils.getResult(result);
        return succeed(userInfoService.updateUserInfo(userInfoDTO));
    }

}
