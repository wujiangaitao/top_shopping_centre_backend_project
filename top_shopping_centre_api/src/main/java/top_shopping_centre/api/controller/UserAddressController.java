package top_shopping_centre.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.dto.user.UserAddressDTO;
import top_shopping_centre.service.UserAddressService;
import top_shopping_centre.util.ComparisonUtils;
import top_shopping_centre.vo.ResponseVO;
import javax.validation.Valid;
import static top_shopping_centre.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 */
@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class UserAddressController {

    private final UserAddressService userAddressService;

    @RequestMapping("/insert/address")
    public ResponseVO insertAddress(@RequestBody @Valid UserAddressDTO userAddressDTO, BindingResult result){
        ComparisonUtils.getResult(result);
        return succeed(userAddressService.insertUserAddress(userAddressDTO));
    }

    @RequestMapping("/list/address")
    public ResponseVO selectUserAddress(@RequestBody UserAddressDTO userAddressDTO){
        return succeed(userAddressService.selectUserAddress(userAddressDTO));
    }

    @RequestMapping("/delete/address")
    public ResponseVO deleteUserAddress(@RequestBody UserAddressDTO userAddressDTO){
        return succeed(userAddressService.deleteUserAddress(userAddressDTO));
    }

    @RequestMapping("/set/address")
    public ResponseVO setUserAddress(@RequestBody UserAddressDTO userAddressDTO){
        return succeed(userAddressService.setTheDefault(userAddressDTO));
    }

    @RequestMapping("/select/address")
    public ResponseVO selectUserAddressDefault(@RequestBody UserAddressDTO userAddressDTO){
        return succeed(userAddressService.selectUserDefaultAddress(userAddressDTO));
    }

}
