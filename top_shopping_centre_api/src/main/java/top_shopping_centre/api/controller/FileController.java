package top_shopping_centre.api.controller;

import io.minio.GetObjectArgs;
import io.minio.GetObjectResponse;
import io.minio.MinioClient;
import io.minio.errors.*;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static top_shopping_centre.util.Constants.minio.*;

/**
 * @author jiangfeng
 */
@RestController
@RequestMapping("/image")
@RequiredArgsConstructor
public class FileController {

    private final MinioClient minioClient;

    /**
     * 从minio中获取图片
     * */
    @RequestMapping("/download/m")
    public ResponseEntity<byte[]> downloadFromMinioM(String objectName) {
        try {
            InputStream inputStream = getMinioClient(BUCKET_NAME_M,objectName);
            byte[] content = getBytes(inputStream);
            HttpHeaders headers = getHttpHeaders(objectName, content);
            return ResponseEntity.ok().headers(headers).body(content);
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("出错啦！",e);
        }
    }

    @RequestMapping("/download/w")
    public ResponseEntity<byte[]> downloadFromMinioW(String objectName) {
        try {
            InputStream inputStream = getMinioClient(BUCKET_NAME_W,objectName);
            byte[] content = getBytes(inputStream);
            HttpHeaders headers = getHttpHeaders(objectName, content);
            return ResponseEntity.ok().headers(headers).body(content);
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("出错啦！"+e);
        }
    }

    @RequestMapping("/download/u")
    public ResponseEntity<byte[]> downloadFromMinioU(String objectName) {
        try {
            InputStream inputStream = getMinioClient(USER_BUCKET,objectName);
            byte[] content = getBytes(inputStream);
            HttpHeaders headers = getHttpHeaders(objectName, content);
            return ResponseEntity.ok().headers(headers).body(content);
        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("出错啦！"+e);
        }
    }

    /**
     * 获取minio连接
     * */
    @NotNull
    private GetObjectResponse getMinioClient(String bucketName, String objectName) throws ErrorResponseException, InsufficientDataException, InternalException, InvalidKeyException, InvalidResponseException, IOException, NoSuchAlgorithmException, ServerException, XmlParserException {
        return minioClient.getObject(GetObjectArgs.builder()
                .bucket(bucketName).object(objectName).build());
    }


    /**
     * 使用 ByteArrayOutputStream 读取输入流到字节数组
     * */
    @NotNull
    private byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        return outputStream.toByteArray();
    }

    /**
     * 设置HTTP头
     * */
    @NotNull
    private static HttpHeaders getHttpHeaders(String objectName, byte[] content) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        headers.setContentLength(content.length);
        headers.setContentDispositionFormData("attachment", objectName);
        return headers;
    }
}
