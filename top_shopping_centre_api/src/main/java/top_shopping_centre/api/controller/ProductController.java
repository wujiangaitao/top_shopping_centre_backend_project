package top_shopping_centre.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.dto.product.ProductDTO;
import top_shopping_centre.service.ProductService;
import top_shopping_centre.vo.ResponseVO;

import static top_shopping_centre.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 * 商品控制层
 */
@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController{


    private final ProductService productService;

    /**
     * 查询所有商品信息
     * */
    @RequestMapping("/select/list")
    public ResponseVO getAllProduct(){
        return succeed(productService.getAllProduct());
    }

    /**
     * 通过Id查询商品信息
     * */
    @RequestMapping("/select/id")
    public ResponseVO getProductById(ProductDTO productDTO){
        return succeed(productService.getProductById(productDTO));
    }

    /**
     * 通过商品Id查询商品颜色
     * */
    @RequestMapping("/select/color")
    public ResponseVO getProductColor(ProductDTO productDTO){
        return succeed(productService.getProductColor(productDTO));
    }

    /**
     * 通过商品Id查询商品图片
     * */
    @RequestMapping("/select/image")
    public ResponseVO getProductImage(ProductDTO productDTO){
        return succeed(productService.getProductAllImage(productDTO));
    }


}
