package top_shopping_centre.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.dto.cat.CatDTO;
import top_shopping_centre.service.CatService;
import top_shopping_centre.vo.ResponseVO;

import static top_shopping_centre.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 * 购物车
 */

@RestController
@RequestMapping("/cat")
@RequiredArgsConstructor
public class CatController {
    private final CatService catService;

    /**
     * 添加购物车
     * */
    @RequestMapping("/insert/cat")
    public ResponseVO insertCat(@RequestBody CatDTO catDTO){
        return succeed(catService.insertCat(catDTO));
    }

    /**
     * 购物车数量就--
     * */
    @RequestMapping("/jan/cat")
    public ResponseVO janCatQuantity(@RequestBody CatDTO catDTO){
        return succeed(catService.updateCatProductJan(catDTO));
    }

    /**
     * 查询购物车所有的信息
     * */
    @RequestMapping("/select/cat")
    public ResponseVO selectCat(@RequestBody CatDTO catDTO){
        return succeed(catService.selectCatAllProduct(catDTO));
    }

    /**
     * 删除购物车信息
     * */
    @RequestMapping("/delete/id")
    public ResponseVO deleteCat(@RequestBody CatDTO catDTO){
        return succeed(catService.deleteCatProduct(catDTO));
    }
}
