package top_shopping_centre.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.dto.user.UserLoginDTO;
import top_shopping_centre.service.UserLoginService;
import top_shopping_centre.util.ComparisonUtils;
import top_shopping_centre.vo.ResponseVO;
import javax.validation.Valid;
import static top_shopping_centre.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 */
@RestController
@RequestMapping("/userLogin")
@RequiredArgsConstructor
public class UserLoginController {
    private final UserLoginService userLoginService;


    @RequestMapping("/login/user")
    public ResponseVO insertUser(@RequestBody @Valid UserLoginDTO userLoginDTO, BindingResult result){
        ComparisonUtils.getResult(result);
        return succeed(userLoginService.loginOrRegister(userLoginDTO));
    }
}
