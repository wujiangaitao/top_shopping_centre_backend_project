package top_shopping_centre.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.dto.order.OrderDTO;
import top_shopping_centre.service.OrderService;
import top_shopping_centre.vo.ResponseVO;
import static top_shopping_centre.vo.EncapsulationResponseVO.succeed;

/**
 * @author jiangfeng
 * 订单控制层
 */
@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    /**
     * 添加订单信息
     * */
    @RequestMapping("/insert/order")
    public ResponseVO insertOrder(@RequestBody OrderDTO orderDTO){
        return succeed(orderService.insertOrder(orderDTO));
    }

    /**
     * 查询订单信息
     * */
    @RequestMapping("/list/order")
    public ResponseVO selectUserOrder(@RequestBody OrderDTO orderDTO){
        return succeed(orderService.userOrderProduct(orderDTO));
    }

    /**
     * 修改订单状态并且修改购物车状态
     * */
    @RequestMapping("/update/order")
    public ResponseVO updateOrder(@RequestBody OrderDTO orderDTO){
        return succeed(orderService.updateOrder(orderDTO));
    }

    /**
     * 查询当前用户的所有订单
     * */
    @RequestMapping("/select/order")
    public ResponseVO selectUserOrderListAndUserId(@RequestBody OrderDTO orderDTO){
        return succeed(orderService.selectUserOrderListAndUserId(orderDTO));
    }

    /**
     * 取消支付
     * */
    @RequestMapping("/cancel/order")
    public ResponseVO userCancelThePaymentOrder(@RequestBody OrderDTO orderDTO){
        return succeed(orderService.userCancelThePaymentOrder(orderDTO));
    }

    /**
     * 用户订单传入订单信息
     * */
    @RequestMapping("/select/id/order")
    public ResponseVO userOrderIdInfo(@RequestBody OrderDTO orderDTO){
        return succeed(orderService.userOrderIdInfo(orderDTO));
    }
}
