package top_shopping_centre.api.controller;


import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top_shopping_centre.dto.pay.PayDTO;
import top_shopping_centre.util.ZFBPayUtils;
import top_shopping_centre.vo.ResponseVO;
import static top_shopping_centre.vo.EncapsulationResponseVO.succeed;


/**
 * @author jiangfeng
 * 支付宝控制层
 */
@RestController
@RequestMapping("/zfb")
public class ZFBController {

    /**
     * 支付宝支付
     * */
    @RequestMapping("/zf")
    public ResponseVO zfb(@RequestBody PayDTO payDTO){
        return succeed(ZFBPayUtils.zfb(payDTO));
    }
}
