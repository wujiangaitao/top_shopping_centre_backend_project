package top_shopping_centre.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top_shopping_centre.interceptor.AuthenticationInterceptor;
import top_shopping_centre.util.JacksonUtils;
import java.util.List;

/**
 * @author jiangfeng
 * 子容器
 */
@Configuration
@EnableWebMvc
@ComponentScan("top_shopping_centre.api.controller")
public class MvcConfig implements WebMvcConfigurer {
    /**
     * 配置日期
     * */
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new DateFormatter("yyyy-MM-dd"));
    }

    /**
     * 消息转换器
     * */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(JacksonUtils.getObjectMapper());
        converters.add(0, converter);
    }

    /**
     * 拦截器
     * */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthenticationInterceptor())
                .order(Ordered.HIGHEST_PRECEDENCE)
                .addPathPatterns("/cat/insert/cat");
    }
}
