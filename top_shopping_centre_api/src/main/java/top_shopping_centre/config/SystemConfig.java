package top_shopping_centre.config;


import org.jetbrains.annotations.NotNull;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;

/**
 * @author jiangfeng
 * 进行DispatcherServletInitializer初始化
 */
public class SystemConfig extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * 父容器
     * */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{AppConfig.class};
    }

    /**
     * 子容器
     * */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{MvcConfig.class};
    }

    /**
     * 拦截所有请求
     * */
    @NotNull
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    /**
     * 文件上传
     */
    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setMultipartConfig
                (new MultipartConfigElement("/Users/jiangfeng/my/load",
                        10240000, 102400000, 1024000));
    }
    /**
     * 注册字符编码过滤器，此过滤器就是针对DispatchServlet而言的
     * 也就是说请求能进到DispatchServlet，此过滤器就生效，不进mvc，此字符编码过滤器就不生效
     * <p>
     * 此过滤器可以解决接收前端传递过来的数据的乱码问题
     */
    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter charEncodingFilter = new CharacterEncodingFilter();
        charEncodingFilter.setEncoding("UTF-8");
        charEncodingFilter.setForceEncoding(true);
        //配置跨域
        MyCorsFilter myCorsFilter=new MyCorsFilter();
        return new Filter[]{charEncodingFilter,myCorsFilter};
    }

}
