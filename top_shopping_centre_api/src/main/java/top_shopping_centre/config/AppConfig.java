package top_shopping_centre.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
/**
 * @author jiangfeng
 * 主配置类
 */
@Configuration
@ComponentScan("top_shopping_centre")
@Import({MybatisConfig.class,MinioConfig.class})
public class AppConfig {}
