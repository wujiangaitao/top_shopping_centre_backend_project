package top_shopping_centre.config;

import com.github.pagehelper.PageInterceptor;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author jiangfeng
 * mybatis配置类
 */
@Configuration
@MapperScan("top_shopping_centre.mapper")
@EnableTransactionManagement
@Import(DataSourceConfig.class)
public class MybatisConfig {

    /**
     * 配置SqlSessionFactory
     * */
    @Bean
    public SqlSessionFactory sqlSessionFactory(DataSource source) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(source);

        PathMatchingResourcePatternResolver loader = new PathMatchingResourcePatternResolver();

        Resource[] resource = loader.getResources("classpath*:mappers/**/*.xml");
        factoryBean.setMapperLocations(resource);

        PageInterceptor pageInterceptor = pageInterceptor();
        factoryBean.setPlugins(pageInterceptor);

        factoryBean.setTypeAliasesPackage("com.entity");
        factoryBean.setConfiguration(configuration());
        return factoryBean.getObject();
    }

    /**
     * 配置下划线改驼峰命名+日志
     * */
    private org.apache.ibatis.session.Configuration configuration() {
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setMapUnderscoreToCamelCase(true);
        configuration.setLogImpl(StdOutImpl.class);
        return configuration;
    }

    /**
     * 配置分页插件
     * */
    private PageInterceptor pageInterceptor() {
        PageInterceptor pageInterceptor = new PageInterceptor();
        Properties properties = new Properties();
        properties.setProperty("supportMethodsArguments", "true");
        properties.setProperty("reasonable", "true");
        properties.setProperty("helperDialect", "mysql");
        pageInterceptor.setProperties(properties);
        return pageInterceptor;

    }

    /**
     * 配置事务
     * */
    @Bean
    public PlatformTransactionManager transactionManager(DataSource source) {
        return new DataSourceTransactionManager(source);
    }
}
