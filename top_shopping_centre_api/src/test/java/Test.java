import com.sun.org.apache.xml.internal.utils.ObjectStack;
import io.minio.GetObjectArgs;
import io.minio.GetObjectResponse;
import io.minio.MinioClient;
import io.minio.StatObjectResponse;
import io.minio.errors.*;
import javafx.beans.binding.Bindings;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;


public class Test {

    private  MinioClient minioClient ;

    public Test() {
        minioClient= MinioClient.builder()
                .endpoint("http://43.139.216.228:9090")
                .credentials("topimage","topkeyll")
                .build();
    }

    public ResponseEntity<byte[]> downloadFromMinio(String bucketName, String objectName) {
        minioClient= MinioClient.builder()
                .endpoint("http://43.139.216.228:9000")
                .credentials("topimage","topkeyll")
                .build();
        try {
            // 获取对象的输入流
            InputStream inputStream = minioClient.getObject(
                    GetObjectArgs.builder()
                            .bucket(bucketName)
                            .object(objectName)
                            .build()
            );

            // 使用 ByteArrayOutputStream 读取输入流到字节数组
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            byte[] content = outputStream.toByteArray();

            // 设置HTTP头
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);
            headers.setContentLength(content.length);
            headers.setContentDispositionFormData("attachment", objectName);

            // 返回响应实体
            return ResponseEntity.ok()
                    .headers(headers)
                    .body(content);

        } catch (MinioException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
            // 处理异常
        }
        return null;
    }

    @org.junit.Test
    public void tes(){
        Test test = new Test();
        ResponseEntity<byte[]> mgimage = test.downloadFromMinio("mgimage", "m.g.mg1.png");
        System.out.println(mgimage);
    }
}

